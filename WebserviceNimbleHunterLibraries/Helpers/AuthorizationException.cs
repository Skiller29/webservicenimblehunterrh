﻿using System;
using System.Runtime.Serialization;

namespace WebServicePautaOnline.Models
{
    [Serializable]
    public class AuthorizationException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public AuthorizationException() { }
        public AuthorizationException(string permission) : base(permission) { }
        public AuthorizationException(string permission, Exception inner) : base(permission, inner) { }

        protected AuthorizationException(
          SerializationInfo info,
          StreamingContext context)
            : base(info, context) { }
    }
}
