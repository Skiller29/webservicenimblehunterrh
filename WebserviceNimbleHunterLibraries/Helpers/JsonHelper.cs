﻿using Newtonsoft.Json;

namespace WebserviceNimbleHunterLibraries.Helpers
{
    public class JsonHelper <T> where T : class
    {
        public static string ObjectToJson(T objectToConvert)
        {
            JsonSerializerSettings _jsonWriter = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            return JsonConvert.SerializeObject(objectToConvert, _jsonWriter);
        }

        public static T JsonToObject(string jsonText)
        {
            var retorno = JsonConvert.DeserializeObject<T>(jsonText);

            return retorno;
        }
    }
}
