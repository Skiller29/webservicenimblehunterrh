﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class KnowledgeLanguages
    {
        public List<KnowledgeLanguage> Languages { get; set; }
        public String Error { get; set; }
    }
}
