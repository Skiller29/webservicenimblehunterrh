﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Vacancy
    {
        public virtual Int32 Id { get; set; }

        public String Ocupation { get; set; }
        public Boolean IsVacancyDeficient { get; set; }
        public Boolean IsVisibleCandidate { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public Boolean HasFields { get; set; }
        public Boolean HasSteps { get; set; }
        public int IdCurrentStep { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public Vacancy() { }
    }
}
