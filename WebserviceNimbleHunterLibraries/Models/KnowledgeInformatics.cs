﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
   public class KnowledgeInformatics
    {
        public List<KnowledgeInformatic> Informatics { get; set; }
        public String Error { get; set; }
    }
}
