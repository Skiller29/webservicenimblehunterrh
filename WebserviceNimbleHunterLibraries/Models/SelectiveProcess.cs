﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class SelectiveProcess
    {
        public Int32 Id { get; set; }

        public String Observation { get; set; }
        public Boolean IsApproved { get; set; }
        public String Candidate { get; set; }
        public String Vacancy { get; set; }
        public String StatusVacancy { get; set; }
        public int IdUser { get; set; }
        public int IdStep { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

    }
}
