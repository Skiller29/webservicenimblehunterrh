﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class KnowledgeLanguage
    {
        public Int32 Id { get; set; }

        public String EnumLanguage { get; set; }
        public String EnumConversation { get; set; }
        public String EnumReading { get; set; }
        public String EnumWriting { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public KnowledgeLanguage() { }
    }
}
