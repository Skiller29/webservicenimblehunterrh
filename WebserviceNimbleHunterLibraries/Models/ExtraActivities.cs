﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
     public class ExtraActivities
    {
        public List<ExtraActivity> Activites { get; set; }
        public String Error { get; set; }
    }
}
