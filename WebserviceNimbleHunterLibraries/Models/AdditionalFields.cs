﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class AdditionalFields
    {
        public List<AdditionalField> Fields { get; set; }
        public String Error { get; set; }

    }
}
