﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Curriculum
    {
        public virtual Int32 Id { get; set; }

        public Boolean WorkedCompany { get; set; }
        public String SectorWorked { get; set; }
        public DateTime AdmitionDateWork { get; set; }
        public DateTime TerminationDateWork { get; set; }
        public String TerminationReasonWork { get; set; }
        public Boolean HasKinshipWorked { get; set; }
        public String EnumDegreeKinship { get; set; }
        public String NameKinship { get; set; }
        public String SectorKinship { get; set; }
        public String PhoneKinship { get; set; }
        public DateTime LastModification { get; set; }
        public DateTime CreationDate { get; set; }
        public String EnumStatusCurriculum { get; set; }
        public String EnumFirstAreaInterest { get; set; }
        public String EnumSecondAreaInterest { get; set; }
        public String EnumThirdAreaInterest { get; set; }
        public String EnumSalaryRequeriments { get; set; }
        public String NameCandidate { get; set; }
        public int IdUser { get; set; }

        public String Success { get; set; }
        public String Error { get; set; }

        public Curriculum() { }
    }
}
