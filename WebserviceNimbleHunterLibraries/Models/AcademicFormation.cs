﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class AcademicFormation
    {
        public  int Id { get; set; }
        public  string TypeFormation { get; set; }
        public  string Country { get; set; }
        public  String Course { get; set; }
        public  String Institute { get; set; }
        public string StatusFormation { get; set; }
        public  DateTime InitialDate { get; set; }
        public  DateTime EndDate { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }

        public string Error { get; set; }
        public String Success { get; set; }
    }
}
