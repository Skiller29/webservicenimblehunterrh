﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class CompleteCurriculum
    {
        public User User { get; set; }
        public List<AcademicFormation> Formations { get; set; }
        public List<ProfessionalCourse> Courses { get; set; }
        public List<ProfessionalExperience> Experiences { get; set; }
        public List<Certificate> Certificates { get; set; }
        public List<KnowledgeInformatic> Informatics { get; set; }
        public List<KnowledgeLanguage> Languages { get; set; }
        public List<ExtraActivity> Activities { get; set; } 
        public Curriculum Curriculum { get; set; }

        public CompleteCurriculum(User usuario, List<AcademicFormation> formations, List<ProfessionalCourse> courses,
            List<ProfessionalExperience> experiences, List<ExtraActivity> activities, List<Certificate> certificates,
            List<KnowledgeInformatic> informatics, List<KnowledgeLanguage> languages, Curriculum curriculum)
        {
            this.User = usuario;
            this.Formations = formations;
            this.Activities = activities;
            this.Certificates = certificates;
            this.Experiences = experiences;
            this.Courses = courses;
            this.Languages = languages;
            this.Informatics = informatics;
            this.Curriculum = curriculum;
        }
    }
}