﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class ProfessionalCourses
    {
            public List<ProfessionalCourse> Courses { get; set; }
            public String Error { get; set; }
        }
}
