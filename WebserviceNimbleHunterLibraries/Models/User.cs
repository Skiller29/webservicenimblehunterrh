﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class User
    {
        public Int32 Id { get; set; }

        public DateTime CreationDate { get; set; }
        public String Cpf { get; set; }
        public String Password { get; set; }
        public String EnumProfile { get; set; }
        public String Token { get; set; }
        public String FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public String Nationality { get; set; }
        public String EnumGenre { get; set; }
        public String EnumMaritalStatus { get; set; }
        public Boolean IsDeficient { get; set; }
        public String Address { get; set; }
        public String UrlPhoto { get; set; }
        public String Cep { get; set; }
        public String Number { get; set; }
        public String Complement { get; set; }
        public String Neighborhood { get; set; }
        public String Email { get; set; }
        public String HomePhone { get; set; }
        public String CellPhone { get; set; }
        public String OptionalPhone { get; set; }
        public bool HasCurriculum { get; set; }
        public int IdCurriculum { get; set; }
        public String State { get; set; }
        public String City { get; set; }

        public String Error { get; set; }

        public User() { }
    }
}
