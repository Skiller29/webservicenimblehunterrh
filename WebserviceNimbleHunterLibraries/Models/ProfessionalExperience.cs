﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class ProfessionalExperience
    {
        public Int32 Id { get; set; }

        public String Company { get; set; }
        public String Occupation { get; set; }
        public String EnumOccupationArea { get; set; }
        public DateTime AdmissionDate { get; set; }
        public DateTime TerminationDate { get; set; }
        public String ImmediateSuperior { get; set; }
        public String CompanyPhone { get; set; }
        public String EnumCountry { get; set; }
        public Double LastSalary { get; set; }
        public String TerminationReason { get; set; }
        public String ActivitiesPerformed { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Error { get; set; }

        public ProfessionalExperience(){ }
    }
}
