﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class AdditionalField
    {
        public Int32 Id { get; set; }
        public String NameField { get; set; }
        public String EnumTypeField { get; set; }
        public String IdVacancy { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

    }
}
