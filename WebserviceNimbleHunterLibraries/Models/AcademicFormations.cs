﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class AcademicFormations
    {
        public List<AcademicFormation> Formations { get; set; }
        public String Error { get; set; }
    }
}
