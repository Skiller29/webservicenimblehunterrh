﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class ProfessionalExperiences
    {
        public List<ProfessionalExperience> Experiences { get; set; }
        public String Error { get; set; }
    }
}
