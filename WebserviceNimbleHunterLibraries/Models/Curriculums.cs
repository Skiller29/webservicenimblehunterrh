﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
     public class Curriculums
    {
        public List<Curriculum> CurriculumsList { get; set; }
        public String Error { get; set; }
    }
}
