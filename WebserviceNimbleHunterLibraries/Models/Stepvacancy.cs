﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Stepvacancy
    {
        public Int32 Id { get; set; }

        public String EnumStep { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime EndTime { get; set; }
        public String EnumStatus { get; set; }
        public int IdVacancy { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }
    }
}
