﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class ProfessionalCourse
    {
        public virtual Int32 Id { get; set; }

        public String Course { get; set; }
        public String EnumCountry { get; set; }
        public String Institute { get; set; }
        public Int32 Workload { get; set; }
        public DateTime ConclusionDate { get; set; }
        public String NameCityCountry { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Error { get; set; }

        public ProfessionalCourse() { }
    }
}
