﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebserviceNimbleHunterLibraries.Models
{
    public static class EndPoints
    {
        #region Usuário

        public const string LoginUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/login";
        public const string CadastrarUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/cadastrarusuario";
        public const string AlterarSenhaUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/alterarsenhausuario";
        public const string DetalhesUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesusuario";
        public const string EditarDadosPessoaisUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editardadospessoais";
        public const string AlterarFotoUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/alterarfotousuario";
        public const string ProcurarUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/procurarusuario";
        #endregion

        #region Currículo
        public const string DetalhesCurriculoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhescurriculo";
        public const string NovoCurriculoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novocurriculo";
        public const string EditarCurriculoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarcurriculo";

        #endregion

        #region Buscar curriculos
        public const string BuscarPorCertificacaoUrl = "http://localhost/nimblehunterrh/api/buscarporcertificacao";
        public const string BuscarPorIdiomaUrl = "http://localhost/nimblehunterrh/api/buscarporidioma";
        public const string BuscarPorInformaticaUrl = "http://localhost/nimblehunterrh/api/buscarporinformatica";
        public const string BuscarPorAtividadeExtraUrl = "http://localhost/nimblehunterrh/api/buscarporatividadeextra";
        public const string BuscarPorCursoProfissionalizanteUrl = "http://localhost/nimblehunterrh/api/buscarporcursoprofissionalizante";
        public const string BuscarPorFormacaoAcademicaUrl = "http://localhost/nimblehunterrh/api/buscarporformacaoacademica";
        public const string BuscarPorExperienciaProfissionalUrl = "http://localhost/nimblehunterrh/api/buscarporexperienciaprofissional";
        public const string BuscarPorDadosPessoaisUrl = "http://localhost/nimblehunterrh/api/buscarpordadospessoais";
        public const string BuscarPorObjetivosUrl = "http://localhost/nimblehunterrh/api/buscarporobjetivos";
        #endregion

        #region Certificação
        public const string DetalhesCertificadoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhescertificado";
        public const string NovoCertificadoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novocertificado";
        public const string EditarCertificadoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarcertificado";
        public const string ExcluirCertificadoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluircertificado";
        public const string ListarCertificacoesUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarcertificacoes";
        #endregion

        #region Formação acadêmica
        public const string DetalhesFormacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesformacao";
        public const string NovoFormacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novoformacao";
        public const string EditarFormacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarformacao";
        public const string ExcluirFormacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluirformacao";
        public const string ListarFormacoesAcademicasUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarformacoesacademicas";
        #endregion

        #region Atividade extra
        public const string DetalhesAtividadeExtraUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesatividadeextra";
        public const string NovoAtividadeExtraUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novoatividadeextra";
        public const string EditarAtividadeExtraUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editaratividadeextra";
        public const string ExcluirAtividadeExtraUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluiratividadeextra";
        public const string ListarAtividadesExtrasUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listaratividadesextras";
        #endregion

        #region Conhecimento em informática
        public const string DetalhesInformaticaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesinformatica";
        public const string NovoInformaticaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novoinformatica";
        public const string EditarInformaticaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarinformatica";
        public const string ExcluirInformaticaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluirinformatica";
        public const string ListarInformaticaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarconhecimentosinformatica";
        #endregion

        #region Conhecimento em idioma
        public const string DetalhesIdiomaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesidioma";
        public const string NovoIdiomaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novoidioma";
        public const string EditarIdiomaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editaridioma";
        public const string ExcluirIdiomaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluiridioma";
        public const string ListarIdiomasUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarconhecimentosidioma";
        #endregion

        #region Experiência profissional
        public const string DetalhesExperienciaProfissionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesexperienciaprofissional";
        public const string NovoExperienciaProfissionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novoexperienciaprofissional";
        public const string EditarExperienciaProfissionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarexperienciaprofissional";
        public const string ExcluirExperienciaProfissionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluirexperienciaprofissional";
        public const string ListarExperienciasProfissionaisUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarexperienciasprofissionais";
        #endregion

        #region Cursos profissionalizantes
        public const string DetalhesCursosProfissionalizantesUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhescursosprofissionalizantes";
        public const string NovoCursoProfissionalizanteUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novocursoprofissionalizante";
        public const string EditarCursoProfissionalizanteUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarcursoprofissionalizante";
        public const string ExcluirCursoProfissionalizanteUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluircursoprofissionalizante";
        public const string ListarCursosProfissionalizantesUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarcursosprofissionalizantes";
        #endregion

        #region Vaga
        public const string DetalhesVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesvagas";
        public const string NovaVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novavaga";
        public const string EditarVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarvaga";
        public const string ExcluirVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluirvaga";
        public const string ListarVagasEmpresaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarvagasempresa";
        public const string ListarVagasDisponiveisEmpresa = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarvagasdisponiveisempresa";
        public const string CandidatarVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/candidatarvaga";

        //ETAPAS
        public const string DetalhesEtapaVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesetapavaga";
        public const string NovaEtapaVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novaetapavaga";
        public const string EditarEtapaVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editaretapavaga";
        public const string ExcluirEtapaVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluiretapavaga";
        public const string ListarEtapasVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listaretapasvaga";
        #endregion

        #region Perguntas adicionais
        public const string DetalhesCampoAdicionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhescampoadicional";
        public const string NovoCampoAdicionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/novocampoadicional";
        public const string EditarCampoAdicionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarcampoadicional";
        public const string ExcluirCampoAdicionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluircampoadicional";
        public const string ListarCamposAdicionaisVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarcamposadicionaisvaga";
        public const string ListarCamposAdicionaisUsuarioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarcamposadicionaisusuario";
        #endregion

        #region Enum
        public const string ListarPaisesUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarpaises";
        public const string ListarNivelIdiomaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarnivelidioma";
        public const string ListarGrausParentescoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listargrausparentesco";
        public const string ListarAreasInteresseUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarareasinteresse";
        public const string ListarGenerosUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listargeneros";
        public const string ListarIdiomasEnumUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listaridiomas";
        public const string ListarNiveisInformaticaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarniveisinformatica";
        public const string ListarAreasNegocioUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarareasnegocio";
        public const string ListarEstadoscivisUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarestadoscivis";
        public const string ListarAreasOcupacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarareasocupacao";
        public const string ListarEtapasVagasUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listaretapasvagas";
        public const string ListarTiposCampoAdicionalUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listartiposcampoadicional";
        public const string ListarTiposAtividadeUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listartiposatividade";
        public const string ListarTiposFormacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listartiposformacao";
        public const string ListarEstadosUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarestados";
        public const string ListarSalariosUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarfaixasalariopretendido";
        public const string ListarStatusFormacaoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarstatusformacao";
        public const string ListarStatusVagaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarstatusvaga";
        public const string ListarCidadesUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarcidadesaction";
        #endregion

        #region Processos seletivos
        public const string DetalhesProcessoSeletivoCandidatoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/detalhesprocessoseletivocandidato";
        public const string ListarProcessosSeletivosCandidatoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/listarprocessosseletivoscandidato";
        public const string ListarProcessosSeletivosEtapaUrl = "http://localhost/nimblehunterrh/api/listarprocessosseletivosetapa";
        public const string ExcluirProcessoSeletivoCandidatoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/excluirprocessoseletivocandidato";
        public const string AprovarCandidatosProximaEtapaUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/aprovarcandidatosproximaetapa";
        public const string ObservacaoProcessoUrl = "http://www.homolog.webroad.com.br/nimblehunterrh/api/editarobservacaoProcesso";
        public const string InserirCandidatoEtapaUrl = "http://localhost/nimblehunterrh/api/aprovarcandidatosproximaetapa";

        #endregion
    }
}