﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class SelectiveProcesses
    {
        public List<SelectiveProcess> Processes { get; set; }
        public String Error { get; set; }
    }
}