﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class AnswersAdditionalField
    {
        public List<AnswerAdditionalField> Answers { get; set; }
        public String Error { get; set; }

    }
}
