﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Enterprise
    {
        public virtual Int32 Id { get; set; }

        public DateTime CreationDate { get; set; }
        public String Name { get; set; }
        public String Cnpj { get; set; }
        public String Token { get; set; }
        public String LogoUrl { get; set; }
        public String Cep { get; set; }
        public String Neighbohood { get; set; }
        public String CompanyWebsite { get; set; }
        public Boolean IsMultinacional { get; set; }
        public String Phone { get; set; }
        public String EnumLineBusiness { get; set; }

        public String Error { get; set; }

        public Enterprise() { }
    }
}
