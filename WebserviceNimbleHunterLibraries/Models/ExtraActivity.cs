﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class ExtraActivity
    {
        public Int32 Id { get; set; }

        public String  EnumTypeActivity { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime EndDate { get; set; }
        public String EnumCountry { get; set; }
        public String Description { get; set; }
        public String NameCityCountry { get; set; }
        public string City { get; set; }
        public String State { get; set; }
        public String Error { get; set; }
        public String Success { get; set; }

        public ExtraActivity() { }
    }
}
