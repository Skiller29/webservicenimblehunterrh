﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Certificates
    {
        public List<Certificate> CertificatesList { get; set; }
        public String Error { get; set; }
    }
}
