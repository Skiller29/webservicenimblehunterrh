﻿using System;
using System.Collections.Generic;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Vacancies
    {
        public List<Vacancy> VacancyList { get; set; }
        public String Error { get; set; }
    }
}
