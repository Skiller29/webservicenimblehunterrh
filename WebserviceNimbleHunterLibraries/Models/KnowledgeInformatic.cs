﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
   public class KnowledgeInformatic
    {

        public Int32 Id { get; set; }
        public String Software { get; set; }
        public String EnumLevel { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public KnowledgeInformatic() { }
    }
}
