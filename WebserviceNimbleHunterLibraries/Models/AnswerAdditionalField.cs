﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class AnswerAdditionalField
    {
        public Int32 Id { get; set; }
        public String Answer { get; set; }
        public String TitleField { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public AnswerAdditionalField() { }

    }
}
