﻿using System;

namespace WebserviceNimbleHunterLibraries.Models
{
    public class Certificate
    {
        public Int32 Id { get; set; }
        public String Certification { get; set; }
        public Int32 NumberOrganClass { get; set; }
        public String OrganIssuer { get; set; }

        public String Error { get; set; }
        public String Success { get; set; }

        public Certificate() { }
    }
}
