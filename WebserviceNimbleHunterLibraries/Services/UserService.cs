﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Schema;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class UserService : BaseService
    {
        public User Login(string cpf, string senha, string chaveEmpresa)
        {
            var url = $"{EndPoints.LoginUrl}?cpf={cpf}&senha={senha}&chaveempresa={chaveEmpresa}";
            return RestService.MakePostRequestHttpClient<User>(url);
        }

        public User Detalhes(string chaveUsuario)
        {
            var url = $"{EndPoints.DetalhesUsuarioUrl}?chaveusuario={chaveUsuario}";
            return RestService.MakeGetRequestHttpClient<User>(url);
        }

        public User Novo(string cpf, string senha, string nome, string chaveEmpresa)
        {
            var url = $"{EndPoints.CadastrarUsuarioUrl}?cpf={cpf}&senha={senha}&nome={nome}&chaveempresa={chaveEmpresa}";
            return RestService.MakePostRequestHttpClient<User>(url);
        }

        public User AlterarSenha(string chaveUsuario, string senhaOriginal, string novaSenha, string chaveEmpresa)
        {
            var url = $"{EndPoints.AlterarSenhaUsuarioUrl}?chaveusuario={chaveUsuario}&senhaoriginal={senhaOriginal}&novasenha={novaSenha}&chaveempresa={chaveEmpresa}";
            return RestService.MakePostRequestHttpClient<User>(url);
        }

        public User EditarDadosPessoais(string chaveUsuario, string nomeCidade, string nome, DateTime dataNascimento,
            string nacionalidade, string genero, string estadoCivil, bool deficiente, string endereco, string cep,
            string numero, string complemento, string bairro, string email, string telefoneResidencial, string celular,
            string telefoneOpcional)
        {
            var url = $"{EndPoints.EditarDadosPessoaisUsuarioUrl}?chaveusuario={chaveUsuario}&nomecidade={nomeCidade}&nome={nome}&datanascimento={dataNascimento:yyyy-MM-dd}&nacionalidade={nacionalidade}&genero={genero}&estadocivil={estadoCivil}&deficiente={deficiente}&endereco={endereco}&cep={cep}&numero={numero}&complemento={complemento}&bairro={bairro}&email={email}&telefoneresidencial={telefoneResidencial}&celular={celular}&telefoneopcional={telefoneOpcional}";
            return RestService.MakePostRequestHttpClient<User>(url);
        }

        public User AlterarFotoUsuario(string chaveUsuario, HttpPostedFileBase arquivo)
        {
            var url = $"{EndPoints.AlterarFotoUsuarioUrl}?chaveusuario={chaveUsuario}";
            var memory = new MemoryStream();
            arquivo.InputStream.CopyTo(memory);
            return RestService.MakePostRequestFile<User>(url,arquivo.FileName,arquivo.ContentType, memory);
        }

        public SelectiveProcesses MeusProcessosSeletivos(string chaveUsuario)
        {
            var url = $"{EndPoints.ListarProcessosSeletivosCandidatoUrl}?chaveusuario={chaveUsuario}";
            return RestService.MakePostRequestHttpClient<SelectiveProcesses>(url);
        }

        public User ProcurarUsuario(string chaveUsuario, int idUsuario)
        {
            var url = $"{EndPoints.ProcurarUsuarioUrl}?chaveusuario={chaveUsuario}&idusuario={idUsuario}";
            return RestService.MakeGetRequestHttpClient<User>(url);
        }

    }
}