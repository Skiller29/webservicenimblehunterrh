﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class ProfessionalCourseService: BaseService
    {
        public ProfessionalCourse Detalhes(string chaveUsuario, int idCurso)
        {
            var url = $"{EndPoints.DetalhesCursosProfissionalizantesUrl}?chaveusuario={chaveUsuario}&idcurso={idCurso}";
            return RestService.MakeGetRequestHttpClient<ProfessionalCourse>(url);
        }

        public ProfessionalCourse Novo(string chaveUsuario, int idCurriculo, string nomeCidade, string cidadePais, string instituicao,
            DateTime dataConclusao, string pais, string curso, int cargaHoraria)
        {
            var url = $"{EndPoints.NovoCursoProfissionalizanteUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&cidadePais={cidadePais}&instituicao={instituicao}&dataconclusao={dataConclusao:yyyy-MM-dd}&pais={pais}&curso={curso}&cargahoraria={cargaHoraria}&nomecidade={nomeCidade}";
            return RestService.MakePostRequestHttpClient<ProfessionalCourse>(url);
        }

        public ProfessionalCourse Editar(string chaveUsuario, int idCurso, string nomeCidade, string cidadePais, string instituicao,
            DateTime dataConclusao, string pais, string curso, int cargaHoraria)
        {
            var url = $"{EndPoints.EditarCursoProfissionalizanteUrl}?chaveusuario={chaveUsuario}&nomecidade={nomeCidade}&idcurso={idCurso}&cidadePais={cidadePais}&instituicao={instituicao}&dataconclusao={dataConclusao:yyyy-MM-dd}&pais={pais}&curso={curso}&cargahoraria={cargaHoraria}";
            return RestService.MakePostRequestHttpClient<ProfessionalCourse>(url);
        }

        public ProfessionalCourse Excluir(string chaveUsuario, int idCurso)
        {
            var url = $"{EndPoints.ExcluirCursoProfissionalizanteUrl}?chaveusuario={chaveUsuario}&idcurso={idCurso}";
            return RestService.MakePostRequestHttpClient<ProfessionalCourse>(url);
        }

        public ProfessionalCourses ListarCursos(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarCursosProfissionalizantesUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<ProfessionalCourses>(url);
        }
    }
}