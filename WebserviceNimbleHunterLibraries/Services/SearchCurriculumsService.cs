﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class SearchCurriculumsService : BaseService
    {
        public Curriculums BuscarCurriculosPorExperienciaProfissional(string chaveUsuario, string companhia, string ocupacao, string areaOcupacao, string pais, string nomeCidadePais, string nomeCidade)
        {
            var url = $"{EndPoints.BuscarPorExperienciaProfissionalUrl}?chaveusuario={chaveUsuario}&companhia={companhia}&ocupacao={ocupacao}&areaocupacao={areaOcupacao}&pais={pais}&nomecidade={nomeCidade}&nomecidadepais={nomeCidadePais}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorFormacaoAcademica(string chaveUsuario, string status, string tipoFormacao, string curso, string instituicao, string pais, string nomeCidadePais, string nomeCidade)
        {
            var url = $"{EndPoints.BuscarPorFormacaoAcademicaUrl}?chaveusuario={chaveUsuario}&status={status}&tipoformacao={tipoFormacao}&curso={curso}&instituicao={instituicao}&pais={pais}&nomecidadepais={nomeCidadePais}&nomecidadade={nomeCidade}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorDadosPessoais(string chaveUsuario, string nome, string genero, string estadoCivil, string nomeCidade, string bairro)
        {
            var url = $"{EndPoints.BuscarPorDadosPessoaisUrl}?chaveusuario={chaveUsuario}&nome={nome}&genero={genero}&estadocivil={estadoCivil}&nomecidade={nomeCidade}&bairro={bairro}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorObjetivos(string chaveUsuario, string areaInteresseUm, string areaInteresseDois, string areaInteresseTres, string salarioPretendido)
        {
            var url = $"{EndPoints.BuscarPorObjetivosUrl}?chaveusuario={chaveUsuario}&areainteresseum={areaInteresseUm}&areainteressedois={areaInteresseDois}&areainteressetres={areaInteresseTres}&salariopretendido={salarioPretendido}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorCursoProfissionalizante(string chaveUsuario, string curso, string instituicao, string pais, string nomeCidadePais, string nomeCidade)
        {
            var url = $"{EndPoints.BuscarPorCursoProfissionalizanteUrl}?chaveusuario={chaveUsuario}&curso={curso}&instituicao={instituicao}&pais={pais}&nomecidadepais={nomeCidadePais}&nomecidade={nomeCidade}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorAtividadeExtra(string chaveUsuario, string tipoAtividade, string pais, string nomeCidadePais, string nomeCidade)
        {
            var url = $"{EndPoints.BuscarPorAtividadeExtraUrl}?chaveusuario={chaveUsuario}&tipoatividade={tipoAtividade}&pais={pais}&nomecidadepais={nomeCidadePais}&nomecidade={nomeCidade}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorInformatica(string chaveUsuario, string software, string nivel)
        {
            var url = $"{EndPoints.BuscarPorInformaticaUrl}?chaveusuario={chaveUsuario}&software={software}&nivel={nivel}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorIdioma(string chaveUsuario, string idioma, string conversacao, string leitura, string escrita)
        {
            var url = $"{EndPoints.BuscarPorIdiomaUrl}?chaveusuario={chaveUsuario}&idioma={idioma}&conversacao={conversacao}&leitura={leitura}&escrita={escrita}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }

        public Curriculums BuscarCurriculosPorCertificacao(string chaveUsuario, string certificacao, string orgaoEmissor, string numeroOrgao)
        {
            var url = $"{EndPoints.BuscarPorCertificacaoUrl}?chaveusuario={chaveUsuario}&certificacao={certificacao}&orgaoemissor={orgaoEmissor}&numeroorgao={numeroOrgao}";
            return RestService.MakeGetRequestHttpClient<Curriculums>(url);
        }
    }
}