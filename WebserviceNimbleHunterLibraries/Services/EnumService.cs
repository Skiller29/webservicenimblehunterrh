﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class EnumService : BaseService
    {
        public List<string> ListarPaises()
        {
            var url = $"{EndPoints.ListarPaisesUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarNivelIdioma()
        {
            var url = $"{EndPoints.ListarNivelIdiomaUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarStatusVaga()
        {
            var url = $"{EndPoints.ListarStatusVagaUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarGrausParentesco()
        {
            var url = $"{EndPoints.ListarGrausParentescoUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarAreasInteresse()
        {
            var url = $"{EndPoints.ListarAreasInteresseUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarGeneros()
        {
            var url = $"{EndPoints.ListarGenerosUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarIdiomas()
        {
            var url = $"{EndPoints.ListarIdiomasEnumUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarNiveisInformatica()
        {
            var url = $"{EndPoints.ListarNiveisInformaticaUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarAreasNegocio()
        {
            var url = $"{EndPoints.ListarAreasNegocioUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarEstadoscivis()
        {
            var url = $"{EndPoints.ListarEstadoscivisUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarAreasOcupacao()
        {
            var url = $"{EndPoints.ListarAreasOcupacaoUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarEtapasVagas()
        {
            var url = $"{EndPoints.ListarEtapasVagasUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarTiposCampoAdicional()
        {
            var url = $"{EndPoints.ListarTiposCampoAdicionalUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarTiposAtividade()
        {
            var url = $"{EndPoints.ListarTiposAtividadeUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<String> ListarTiposFormacao()
        {
            var url = $"{EndPoints.ListarTiposFormacaoUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarEstados()
        {
            var url = $"{EndPoints.ListarEstadosUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarSalarios()
        {
            var url = $"{EndPoints.ListarSalariosUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarStatusFormacao()
        {
            var url = $"{EndPoints.ListarStatusFormacaoUrl}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }

        public List<string> ListarCidades(string estado)
        {
            var url = $"{EndPoints.ListarCidadesUrl}?estado={estado}";
            return RestService.MakeGetRequestHttpClient<List<string>>(url);
        }
    }
}