﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class KnowledgeLanguageService : BaseService
    {
        public KnowledgeLanguage Detalhes(string chaveUsuario, int idIdioma)
        {
            var url = $"{EndPoints.DetalhesIdiomaUrl}?chaveusuario={chaveUsuario}&ididioma={idIdioma}";
            return RestService.MakeGetRequestHttpClient<KnowledgeLanguage>(url);
        }

        public KnowledgeLanguage Novo(string chaveUsuario, int idCurriculo, string idioma, string nivelConversacao,
            string nivelLeitura, string nivelEscrita)
        {
            var url = $"{EndPoints.NovoIdiomaUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&idioma={idioma}&nivelconversacao={nivelConversacao}&nivelleitura={nivelLeitura}&nivelescrita={nivelEscrita}";
            return RestService.MakePostRequestHttpClient<KnowledgeLanguage>(url);
        }

        public KnowledgeLanguage Editar(string chaveUsuario, int idIdioma, string idioma, string nivelConversacao,
           string nivelLeitura, string nivelEscrita)
        {
            var url = $"{EndPoints.EditarIdiomaUrl}?chaveusuario={chaveUsuario}&ididioma={idIdioma}&nivelconversacao={nivelConversacao}&idioma={idioma}&nivelleitura={nivelLeitura}&nivelescrita={nivelEscrita}";
            return RestService.MakePostRequestHttpClient<KnowledgeLanguage>(url);
        }

        public KnowledgeLanguage Excluir(string chaveUsuario, int idIdioma)
        {
            var url = $"{EndPoints.ExcluirIdiomaUrl}?chaveusuario={chaveUsuario}&ididioma={idIdioma}";
            return RestService.MakePostRequestHttpClient<KnowledgeLanguage>(url);
        }

        public KnowledgeLanguages ListarIdiomas(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarIdiomasUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<KnowledgeLanguages>(url);
        }
    }
}