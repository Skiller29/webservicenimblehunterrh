﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class ExtraActivityService : BaseService
    {
        public ExtraActivity DetalhesAtividade(string chaveUsuario, int idAtividadeExtra)
        {
            var url = $"{EndPoints.DetalhesAtividadeExtraUrl}?chaveusuario={chaveUsuario}&idatividadeextra={idAtividadeExtra}";
            return RestService.MakeGetRequestHttpClient<ExtraActivity>(url);
        }

        public ExtraActivity Novo(string chaveUsuario, int idCurriculo, string cidadePais, string nomeCidade,
            string tipoAtividade, DateTime dataInicial, DateTime dataFinal, string pais, string descricao)
        {
            var url = $"{EndPoints.NovoAtividadeExtraUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&cidadepais={cidadePais}&nomecidade={nomeCidade}&tipoatividade={tipoAtividade}&datainicial={dataInicial:yyyy-MM-dd}&datafinal={dataFinal:yyyy-MM-dd}&pais={pais}&descricao={descricao}";
            return RestService.MakePostRequestHttpClient<ExtraActivity>(url);
        }

        public ExtraActivity Editar(string chaveUsuario, int idAtividadeExtra, string cidadePais, string nomeCidade,
            string tipoAtividade, DateTime dataInicial, DateTime dataFinal, string pais, string descricao)
        {
            var url = $"{EndPoints.EditarAtividadeExtraUrl}?chaveusuario={chaveUsuario}&idatividadeextra={idAtividadeExtra}&cidadepais={cidadePais}&nomecidade={nomeCidade}&tipoatividade={tipoAtividade}&datainicial={dataInicial:yyyy-MM-dd}&datafinal={dataFinal:yyyy-MM-dd}&pais={pais}&descricao={descricao}";
            return RestService.MakePostRequestHttpClient<ExtraActivity>(url);
        }

        public Certificate Excluir(string chaveUsuario, int idAtividadeExtra)
        {
            var url = $"{EndPoints.ExcluirAtividadeExtraUrl}?chaveusuario={chaveUsuario}&idatividadeextra={idAtividadeExtra}";
            return RestService.MakePostRequestHttpClient<Certificate>(url);
        }

        public ExtraActivities ListarAtividades(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarAtividadesExtrasUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<ExtraActivities>(url);
        }
    }
}