﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class ProfessionalExperienceService: BaseService
    {
        public ProfessionalExperience Detalhes(string chaveUsuario, int idExperienciaProfissional)
        {
            var url = $"{EndPoints.DetalhesExperienciaProfissionalUrl}?chaveusuario={chaveUsuario}&idexperienciaprofissional={idExperienciaProfissional}";
            return RestService.MakeGetRequestHttpClient<ProfessionalExperience>(url);
        }

        public ProfessionalExperience Novo(string chaveUsuario, int idCurriculo, string cidadePais, string nomeCidade,
            string pais, string empresa, string contatoEmpresa, string atividades, string areaOcupacao,
            double ultimoSalario, string motivoDesligamento, DateTime dataAdmissao, DateTime dataDesligamento,
            string ocupacao, string superiorImediato)
        {
            var url = $"{EndPoints.NovoExperienciaProfissionalUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&cidadepais={cidadePais}&nomecidade={nomeCidade}&pais={pais}&empresa={empresa}&contatoempresa={contatoEmpresa}&atividades={atividades}&areaocupacao={areaOcupacao}&ultimosalario={ultimoSalario}&motivodesligamento={motivoDesligamento}&dataadmissao={dataAdmissao:yyyy-MM-dd}&datadesligamento={dataDesligamento:yyyy-MM-dd}&ocupacao={ocupacao}&superiorimediato={superiorImediato}";
            return RestService.MakePostRequestHttpClient<ProfessionalExperience>(url);
        }

        public ProfessionalExperience Editar(string chaveUsuario, int idExperienciaProfissional, string cidadePais, string nomeCidade,
            string pais, string empresa, string contatoEmpresa, string atividades, string areaOcupacao,
            double ultimoSalario, string motivoDesligamento, DateTime dataAdmissao, DateTime dataDesligamento,
            string ocupacao, string superiorImediato)
        {
            var url = $"{EndPoints.EditarExperienciaProfissionalUrl}?chaveusuario={chaveUsuario}&idexperienciaprofissional={idExperienciaProfissional}&cidadepais={cidadePais}&nomecidade={nomeCidade}&pais={pais}&empresa={empresa}&contatoempresa={contatoEmpresa}&atividades={atividades}&areaocupacao={areaOcupacao}&ultimosalario={ultimoSalario}&motivodesligamento={motivoDesligamento}&dataadmissao={dataAdmissao}&datadesligamento={dataDesligamento}&ocupacao={ocupacao}&superiorimediato={superiorImediato}";
            return RestService.MakePostRequestHttpClient<ProfessionalExperience>(url);
        }

        public ProfessionalExperience Excluir(string chaveUsuario, int idExperienciaProfissional)
        {
            var url = $"{EndPoints.ExcluirExperienciaProfissionalUrl}?chaveusuario={chaveUsuario}&idexperienciaprofissional={idExperienciaProfissional}";
            return RestService.MakePostRequestHttpClient<ProfessionalExperience>(url);
        }

        public ProfessionalExperiences ListarExperienciasProfissionais(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarExperienciasProfissionaisUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<ProfessionalExperiences>(url);
        }
    }
}