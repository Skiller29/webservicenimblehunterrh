﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class AcademicFormationService: BaseService
    {
        public AcademicFormation Detalhes(string chaveUsuario, int idFormacao) 
        {
            var url = $"{EndPoints.DetalhesFormacaoUrl}?chaveusuario={chaveUsuario}&idformacao={idFormacao}";
            return RestService.MakeGetRequestHttpClient<AcademicFormation>(url);
        }

        public AcademicFormation Novo(string chaveUsuario, int idCurriculo, string cidadePais, string nomeCidade,
            string instituicao, string status, string tipoFormacao, DateTime dataInicial, DateTime dataFinal,
            string pais, string curso)
        {
            var url = $"{EndPoints.NovoFormacaoUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&nomecidade={nomeCidade}&instituicao={instituicao}&status={status}&tipoformacao={tipoFormacao}&datainicial={dataInicial:yyyy-MM-dd}&datafinal={dataFinal:yyyy-MM-dd}&cidadepais={cidadePais}&pais={pais}&curso={curso}";
            return RestService.MakePostRequestHttpClient<AcademicFormation>(url);
        }

        public AcademicFormation Editar(string chaveUsuario, int idFormacao, string cidadePais, string nomeCidade,
           string instituicao, string status, string tipoFormacao, DateTime dataInicial, DateTime dataFinal,
           string pais, string curso)
        {
            var url = $"{EndPoints.EditarFormacaoUrl}?chaveusuario={chaveUsuario}&idformacao={idFormacao}&nomecidade={nomeCidade}&instituicao={instituicao}&status={status}&tipoformacao={tipoFormacao}&datainicial={dataInicial:yyyy-MM-dd}&datafinal={dataFinal:yyyy-MM-dd}&cidadepais={cidadePais}&pais={pais}&curso={curso}";
            return RestService.MakePostRequestHttpClient<AcademicFormation>(url);
        }

        public AcademicFormation Excluir(string chaveUsuario, int idFormacao)
        {
            var url = $"{EndPoints.ExcluirFormacaoUrl}?chaveusuario={chaveUsuario}&idformacao={idFormacao}";
            return RestService.MakePostRequestHttpClient<AcademicFormation>(url);
        }

        public AcademicFormations ListarFormacoes(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarFormacoesAcademicasUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<AcademicFormations>(url);
        }
    }
}