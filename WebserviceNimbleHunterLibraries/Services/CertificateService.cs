﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class CertificateService : BaseService
    {
        public Certificate DetalhesCertificado(string chaveUsuario, int idCertificado)
        {
            var url = $"{EndPoints.DetalhesCertificadoUrl}?chaveusuario={chaveUsuario}&idcertificado={idCertificado}";
            return RestService.MakeGetRequestHttpClient<Certificate>(url);
        }

        public Certificate Novo(string chaveUsuario, int idCurriculo, string certificacao, int numeroOrgaoClasse, string orgaoEmissor)
        {
            var url = $"{EndPoints.NovoCertificadoUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&certificacao={certificacao}&numeroorgaoclasse={numeroOrgaoClasse}&orgaoemissor={orgaoEmissor}";
            return RestService.MakePostRequestHttpClient<Certificate>(url);
        }

        public Certificate Editar(string chaveUsuario, int idCertificado, string certificacao, int numeroOrgaoClasse, string orgaoEmissor)
        {
            var url = $"{EndPoints.EditarCertificadoUrl}?chaveusuario={chaveUsuario}&idcertificado={idCertificado}&certificacao={certificacao}&numeroorgaoclasse={numeroOrgaoClasse}&orgaoemissor={orgaoEmissor}";
            return RestService.MakePostRequestHttpClient<Certificate>(url);
        }

        public Certificate Excluir(string chaveUsuario, int idCertificado)
        {
            var url = $"{EndPoints.ExcluirCertificadoUrl}?chaveusuario={chaveUsuario}&idcertificado={idCertificado}";
            return RestService.MakePostRequestHttpClient<Certificate>(url);
        }

        public Certificates ListarCertificacoes(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarCertificacoesUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<Certificates>(url);
        }
    }
}