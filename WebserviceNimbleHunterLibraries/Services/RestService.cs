﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class RestService
    {
        public string Response { get; set; }

        public T MakeGetRequestHttpClient<T>(string url)
        {
            using (var client = new HttpClient())
            {
                var response = client.GetStringAsync(url).Result;
                return JsonConvert.DeserializeObject<T>(response);
            }
        }

        public T MakePostRequestHttpClient<T>(string url, Dictionary<string, string> parameters = null)
        {
            using (var client = new HttpClient())
            {
                var builder = new StringBuilder();
                if (parameters != null && parameters.Any())
                {
                    foreach (var parameter in parameters)
                        builder.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);
                }

                var response = client.PostAsync(url, new StringContent(builder.ToString().Trim('&'), Encoding.Default)).Result;
                return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            }
        }

        public T MakeGetRequestWebClient<T>(string url)
        {
            using (var client = new WebClient())
            {
                var response = client.DownloadString(url);
                return JsonConvert.DeserializeObject<T>(response);
            }
        }

        public T MakePostRequestWebClient<T>(string url, Dictionary<string, string> parameters = null)
        {
            using (var client = new WebClient())
            {
                var builder = new StringBuilder();
                if (parameters != null && parameters.Any())
                {
                    foreach (var parameter in parameters)
                        builder.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);
                }

                var response = client.UploadString(url, builder.ToString().Trim('&'));
                return JsonConvert.DeserializeObject<T>(response);
            }
        }

        public T MakePostRequestFile<T>(string url, string fileName, string contentType, MemoryStream stream)
        {
            using (var client = new HttpClient())
            {
                var form = new MultipartFormDataContent();
                var attachment = new ByteArrayContent(stream.ToArray(), 0, stream.ToArray().Length);
                attachment.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                form.Add(attachment, contentType.Split('/')[0], fileName);

                var response = client.PostAsync(url, form).Result;
                return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            }
        }
    }
}
