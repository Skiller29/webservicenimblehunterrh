﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class KnowledgeInformaticService: BaseService
    {
        public KnowledgeInformatic DetalhesInformatica(string chaveUsuario, int idInformatica)
        {
            var url = $"{EndPoints.DetalhesInformaticaUrl}?chaveusuario={chaveUsuario}&idinformatica={idInformatica}";
            return RestService.MakeGetRequestHttpClient<KnowledgeInformatic>(url);
        }

        public KnowledgeInformatic Novo(string chaveUsuario, int idCurriculo, string software, string nivel)
        {
            var url = $"{EndPoints.NovoInformaticaUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&software={software}&nivel={nivel}";
            return RestService.MakePostRequestHttpClient<KnowledgeInformatic>(url);
        }

        public KnowledgeInformatic Editar(string chaveUsuario, int idInformatica, string software, string nivel)
        {
            var url = $"{EndPoints.EditarInformaticaUrl}?chaveusuario={chaveUsuario}&idinformatica={idInformatica}&software={software}&nivel={nivel}";
            return RestService.MakePostRequestHttpClient<KnowledgeInformatic>(url);
        }

        public KnowledgeInformatic Excluir(string chaveUsuario, int idInformatica)
        {
            var url = $"{EndPoints.ExcluirInformaticaUrl}?chaveusuario={chaveUsuario}&idinformatica={idInformatica}";
            return RestService.MakePostRequestHttpClient<KnowledgeInformatic>(url);
        }

        public KnowledgeInformatics ListarInformatica(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.ListarInformaticaUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<KnowledgeInformatics>(url);
        }
    }
}