﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class CurriculumService : BaseService
    {
        public Curriculum Detalhes(string chaveUsuario, int idCurriculo)
        {
            var url = $"{EndPoints.DetalhesCurriculoUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}";
            return RestService.MakeGetRequestHttpClient<Curriculum>(url);
        }

        public Curriculum Novo(string chaveUsuario, bool trabalhouEmpresa, string setorTrabalhado, DateTime dataAdmissaoTrabalhado, DateTime dataDesligamentoTrabalhado, string motivoDesligamentoTrabalhado, bool parenteEmpresa, string tipoParentesco, string nomeParente, string setorParente, string telefoneParente, string areaInteresseUm, string areaInteresseDois, string areaInteresseTres, string salarioPretendido)
        {
            var url = $"{EndPoints.NovoCurriculoUrl}?chaveusuario={chaveUsuario}&trabalhouempresa={trabalhouEmpresa}&setortrabalhado={setorTrabalhado}&dataadmissaotrablhado={dataAdmissaoTrabalhado}&datadesligamentotrabalhado={dataDesligamentoTrabalhado}&motivodesligamentotrabalhado={motivoDesligamentoTrabalhado}&parenteempresa={parenteEmpresa}&tipoparentesco={tipoParentesco}&nomeparente={nomeParente}&setorparente={setorParente}&telefoneparente={telefoneParente}&areainteresseum={areaInteresseUm}&areainteressedois={areaInteresseDois}&areainteressetres={areaInteresseTres}&salariopretendido={salarioPretendido}";
            return RestService.MakePostRequestHttpClient<Curriculum>(url);
        }

        public Curriculum Editar(string chaveUsuario, int idCurriculo, bool trabalhouEmpresa, string setorTrabalhado, DateTime dataAdmissaoTrabalhado, DateTime dataDesligamentoTrabalhado, string motivoDesligamentoTrabalhado, bool parenteEmpresa, string tipoParentesco, string nomeParente, string setorParente, string telefoneParente, string areaInteresseUm, string areaInteresseDois, string areaInteresseTres, string salarioPretendido)
        {
            var url = $"{EndPoints.EditarCurriculoUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&trabalhouempresa={trabalhouEmpresa}&setortrabalhado={setorTrabalhado}&dataadmissaotrablhado={dataAdmissaoTrabalhado}&datadesligamentotrabalhado={dataDesligamentoTrabalhado}&motivodesligamentotrabalhado={motivoDesligamentoTrabalhado}&parenteempresa={parenteEmpresa}&tipoparentesco={tipoParentesco}&nomeparente={nomeParente}&setorparente={setorParente}&telefoneparente={telefoneParente}&areainteresseum={areaInteresseUm}&areainteressedois={areaInteresseDois}&areainteressetres={areaInteresseTres}&salariopretendido={salarioPretendido}";
            return RestService.MakePostRequestHttpClient<Curriculum>(url);
        }
    }
}