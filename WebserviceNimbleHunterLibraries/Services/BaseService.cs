﻿namespace WebserviceNimbleHunterLibraries.Services
{
    public abstract class BaseService
    {
        protected RestService RestService { get; set; }

        protected BaseService()
        {
            RestService = new RestService();
        }
    }
}
