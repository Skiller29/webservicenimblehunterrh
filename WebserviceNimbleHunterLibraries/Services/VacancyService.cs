﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class VacancyService : BaseService
    {
        public Vacancy Detalhes(string chaveUsuario, int idVaga)
        {
            var url = $"{EndPoints.DetalhesVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}";
            return RestService.MakePostRequestHttpClient<Vacancy>(url);
        }

        public Vacancy Novo(string chaveUsuario, string nomeCidade, string ocupacao, bool deficiente, bool visivel, string descricao, string chaveEmpresa)
        {
            var url = $"{EndPoints.NovaVagaUrl}?chaveusuario={chaveUsuario}&chaveempresa={chaveEmpresa}&nomecidade={nomeCidade}&ocupacao={ocupacao}&vagadeficiente={deficiente}&visivelcandidato={visivel}&descricao={descricao}";
            return RestService.MakePostRequestHttpClient<Vacancy>(url);
        }

        public Vacancy Editar(string chaveUsuario, string nomeCidade, string ocupacao, bool deficiente, bool visivel, string descricao, int idVaga)
        {
            var url = $"{EndPoints.EditarVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}&nomecidade={nomeCidade}&ocupacao={ocupacao}&vagadeficiente={deficiente}&visivelcandidato={visivel}&descricao={descricao}";
            return RestService.MakePostRequestHttpClient<Vacancy>(url);
        }

        public Vacancy Excluir(string chaveUsuario, int idVaga)
        {
            var url = $"{EndPoints.ExcluirVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}";
            return RestService.MakePostRequestHttpClient<Vacancy>(url);
        }

        public Vacancies ListarVagasEmpresa(string chaveUsuario, string chaveEmpresa)
        {
            var url = $"{EndPoints.ListarVagasEmpresaUrl}?chaveusuario={chaveUsuario}&chaveempresa={chaveEmpresa}";
            return RestService.MakePostRequestHttpClient<Vacancies>(url);
        }

        public Vacancies ListarVagasDisponiveis(string chaveEmpresa, string chaveUsuario)
        {
            var url = $"{EndPoints.ListarVagasDisponiveisEmpresa}?chaveempresa={chaveEmpresa}&chaveusuario={chaveUsuario}";
            return RestService.MakePostRequestHttpClient<Vacancies>(url);
        }

        public SelectiveProcess CandidatarVaga(string chaveUsuario, int idVaga, List<string> respostas = null)
        {
            var url = $"{EndPoints.CandidatarVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}";
            if (respostas != null && respostas.Any())
            {
                for (int i = 0; i < respostas.Count; i++)
                {
                    url += $"&respostas={respostas[i]}";
                }
            }
            return RestService.MakePostRequestHttpClient<SelectiveProcess>(url);
        }

    //ETAPAS
    public Stepvacancy DetalhesEtapa(string chaveUsuario, int idEtapa)
    {
        var url = $"{EndPoints.DetalhesEtapaVagaUrl}?chaveusuario={chaveUsuario}&idetapa={idEtapa}";
        return RestService.MakePostRequestHttpClient<Stepvacancy>(url);
    }

    public Stepvacancy NovaEtapa(string chaveUsuario, int idVaga, string tipoEtapa, DateTime dataInicial, DateTime dataFinal)
    {
        var url = $"{EndPoints.NovaEtapaVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}&tipoetapa={tipoEtapa}&datainicial={dataInicial:yyyy-MM-dd}&datafinal={dataFinal:yyyy-MM-dd}";
        return RestService.MakePostRequestHttpClient<Stepvacancy>(url);
    }

    public Stepvacancy EditarEtapa(string chaveUsuario, int idEtapa, string tipoEtapa, DateTime dataInicial, DateTime dataFinal)
    {
        var url = $"{EndPoints.EditarEtapaVagaUrl}?chaveusuario={chaveUsuario}&idetapa={idEtapa}&tipoetapa={tipoEtapa}&datainicial={dataInicial:yyyy-MM-dd}&datafinal={dataFinal:yyyy-MM-dd}";
        return RestService.MakePostRequestHttpClient<Stepvacancy>(url);
    }

    public Stepvacancy ExcluirEtapa(string chaveUsuario, int idEtapa)
    {
        var url = $"{EndPoints.ExcluirEtapaVagaUrl}?chaveusuario={chaveUsuario}&idetapa={idEtapa}";
        return RestService.MakePostRequestHttpClient<Stepvacancy>(url);
    }

    public StepsVacancy ListarEtapas(string chaveUsuario, int idVaga)
    {
        var url = $"{EndPoints.ListarEtapasVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}";
        return RestService.MakePostRequestHttpClient<StepsVacancy>(url);
    }

}
}