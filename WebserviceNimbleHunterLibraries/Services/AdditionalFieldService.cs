﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class AdditionalFieldService : BaseService
    {
        public AdditionalField Detalhes(string chaveUsuario, int idCampo)
        {
            var url = $"{EndPoints.DetalhesCampoAdicionalUrl}?chaveusuario={chaveUsuario}&idcampo={idCampo}";
            return RestService.MakePostRequestHttpClient<AdditionalField>(url);
        }

        public AdditionalField Novo(string chaveUsuario, int idVaga, string tituloCampo, string tipoCampo)
        {
            var url = $"{EndPoints.NovoCampoAdicionalUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}&titulocampo={tituloCampo}&tipocampo={tipoCampo}";
            return RestService.MakePostRequestHttpClient<AdditionalField>(url);
        }

        public AdditionalField Editar(string chaveUsuario, int idCampo, string tituloCampo, string tipoCampo)
        {
            var url = $"{EndPoints.EditarCampoAdicionalUrl}?chaveusuario={chaveUsuario}&idcampo={idCampo}&titulocampo={tituloCampo}&tipocampo={tipoCampo}";
            return RestService.MakePostRequestHttpClient<AdditionalField>(url);
        }

        public AdditionalField Excluir(string chaveUsuario, int idCampo)
        {
            var url = $"{EndPoints.ExcluirCampoAdicionalUrl}?chaveusuario={chaveUsuario}&idcampo={idCampo}";
            return RestService.MakePostRequestHttpClient<AdditionalField>(url);
        }

        public AdditionalFields ListarCamposVaga(string chaveUsuario, int idVaga)
        {
            var url = $"{EndPoints.ListarCamposAdicionaisVagaUrl}?chaveusuario={chaveUsuario}&idvaga={idVaga}";
            return RestService.MakePostRequestHttpClient<AdditionalFields>(url);
        }

        public AdditionalFields ListarCamposUsuario(string chaveUsuario)
        {
            var url = $"{EndPoints.ListarCamposAdicionaisUsuarioUrl}?chaveusuario={chaveUsuario}";
            return RestService.MakePostRequestHttpClient<AdditionalFields>(url);
        }
    }
}