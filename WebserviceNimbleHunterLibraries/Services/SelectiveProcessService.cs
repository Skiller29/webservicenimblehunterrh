﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebserviceNimbleHunterLibraries.Models;

namespace WebserviceNimbleHunterLibraries.Services
{
    public class SelectiveProcessService : BaseService
    {
        public SelectiveProcess Detalhes(string chaveUsuario, int idProcesso)
        {
            var url = $"{EndPoints.DetalhesProcessoSeletivoCandidatoUrl}?chaveusuario={chaveUsuario}&idprocesso={idProcesso}";
            return RestService.MakeGetRequestHttpClient<SelectiveProcess>(url);
        }

        public SelectiveProcess Excluir(string chaveUsuario, int idProcesso)
        {
            var url = $"{EndPoints.ExcluirProcessoSeletivoCandidatoUrl}?chaveusuario={chaveUsuario}&idprocesso={idProcesso}";
            return RestService.MakeGetRequestHttpClient<SelectiveProcess>(url);
        }

        public SelectiveProcesses ListarProcessosEtapa(string chaveUsuario, int idEtapa)
        {
            var url = $"{EndPoints.ListarProcessosSeletivosEtapaUrl}?chaveusuario={chaveUsuario}&idetapa={idEtapa}";
            return RestService.MakeGetRequestHttpClient<SelectiveProcesses>(url);
        }

        public SelectiveProcesses AprovarCandidatos(string chaveUsuario, int idEtapaAtual, int? idProximaEtapa, List<string> idAprovados, bool enviarEmail, string mensagemAprovados, string mensagemReprovados)
        {
            var url = $"{EndPoints.AprovarCandidatosProximaEtapaUrl}?chaveusuario={chaveUsuario}&idetapa={idEtapaAtual}&idproximaetapa={idProximaEtapa}&enviaremail={enviarEmail}&mensagemaprovados={mensagemAprovados}&mensagemreprovados={mensagemReprovados}";
            for (int i = 0; i < idAprovados.Count; i++)
            {
                url += string.Format("&idcandidatosaprovados={0}", idAprovados[i]);
            }
            return RestService.MakePostRequestHttpClient<SelectiveProcesses>(url);
        }

        public SelectiveProcess Observacao(string chaveUsuario, int idProcesso, string observacao)
        {
            var url = $"{EndPoints.ObservacaoProcessoUrl}?chaveusuario={chaveUsuario}&idprocesso={idProcesso}&observacao={observacao}";
            return RestService.MakePostRequestHttpClient<SelectiveProcess>(url);
        }

        public SelectiveProcess InserirCandidatoEtapa(string chaveUsuario, int idCurriculo, int idEtapa)
        {
            var url = $"{EndPoints.InserirCandidatoEtapaUrl}?chaveusuario={chaveUsuario}&idcurriculo={idCurriculo}&idetapa={idEtapa}";
            return RestService.MakePostRequestHttpClient<SelectiveProcess>(url);
        }
    }
}