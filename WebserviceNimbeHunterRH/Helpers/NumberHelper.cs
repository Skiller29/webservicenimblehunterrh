﻿using System;

namespace WebServicePautaOnline.Helpers
{
    public static class NumberHelper
    {
        public static int ToMB(this string str)
        {
            if (string.IsNullOrWhiteSpace(str) || str == "0") return 0;

            try
            {
                return (int) (long.Parse(str.Split('.')[0])/1000000000000);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
