﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using WebServicePautaOnline.Models;

namespace WebserviceNimbeHunterRH.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class RequiresAuthorizationAttribute : ActionFilterAttribute, IAuthorizationFilter, IExceptionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var identity = filterContext.HttpContext.User.Identity;

            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            bool isLoggedIn = identity.IsAuthenticated;

            if (!isLoggedIn)
            {
                NaoLogado(filterContext);
                return;
            }

            base.OnActionExecuting(filterContext);
        }

        private static void NaoLogado(ActionExecutingContext filterContext)
        {
            //var rUrl = filterContext.HttpContext.Request.Url.AbsoluteUri.Replace("/Web_deploy", "");

            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "controller", "Membros" }, 
                    { "action", "Login" } 
                });


            //new UrlHelper(filterContext.RequestContext).Action(MVC.Membros.Login(rUrl)));
        }

        private static void AcessoNegado(ExceptionContext filterContext)
        {
            //var rUrl = filterContext.HttpContext.Request.Url.AbsoluteUri.Replace("/Web_deploy", "");

            //filterContext.HttpContext.Response.Redirect(
            //    new UrlHelper(filterContext.RequestContext).Action(MVC.Membros.Login(rUrl)));

            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "controller", "Membros" }, 
                    { "action", "Login" } 
                });
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
        }

        #region IExceptionFilter Members

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is AuthorizationException)
                AcessoNegado(filterContext);
        }

        #endregion
    }
}