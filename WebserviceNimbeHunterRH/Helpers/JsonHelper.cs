﻿using Newtonsoft.Json;

namespace WebServicePautaOnline.Helpers
{
    public static class JsonHelper
    {
        public static string ToJSON(this object obj, bool debug = false, bool lowerCase = false)
        {
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            //if (lowerCase) settings.ContractResolver = new LowercaseContractResolver();
            return JsonConvert.SerializeObject(obj, debug ? Formatting.Indented : Formatting.None, settings);
        }
    }
}
