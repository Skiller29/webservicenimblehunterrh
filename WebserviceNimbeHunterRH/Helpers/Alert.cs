﻿namespace WebserviceNimbeHunterRH.Helpers
{
    public class Alert
    {
        public string Type { get; set; }
        public string Message { get; set; }

        public Alert(string type, string message)
        {
            this.Type = type;
            this.Message = message;
        }
    }
}
