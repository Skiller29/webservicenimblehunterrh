﻿using System;
using System.Web.Mvc;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Helpers
{
    public class LoadDataAttribute : ActionFilterAttribute
    {
        //Executar quando a action está iniciando sua execução, ou seja, antes ainda
        //do que está dentro dos { } dela
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var viewResult = filterContext.Controller;

            if (viewResult == null)
                return;

            //Checagem simples para ver se há alguém autenticado
            var userIdentity = filterContext.HttpContext.User.Identity;

            //Tem alguém autentiado e logado
            if (userIdentity != null && userIdentity.IsAuthenticated)
            {
                try
                {
                    //Carregar os dados básicos do membro pelo webservice e armazenar numa viewbag
                    //Assim a propria action que usa o LoadData e a view que ela renderiza pode acessar essa viewbag e checar se há membro pré-carregado
                    var usuario = new UserService().Detalhes(userIdentity.Name);
                    viewResult.ViewBag.Usuario = usuario;
                }
                catch (Exception)
                {
                }
            }

            //Tendo ou não membro logado, definimos aqui algumas viewbags que vão nos ajudar com mensagens de alerta ou marcação de menus
            viewResult.ViewBag.Alerta = viewResult.TempData["Alerta"];
            viewResult.ViewBag.MenuTab = viewResult.TempData["MenuTab"];
            viewResult.ViewBag.SubMenuTab = viewResult.TempData["SubMenuTab"];
            viewResult.ViewBag.ReturnUrl = viewResult.TempData["ReturnUrl"];

            //Continua o fluxo normal da action
            base.OnActionExecuting(filterContext);
        }
    }
}