﻿using System;

namespace WebserviceNimbeHunterRH.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime GetCurrent(this DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime, "E. South America Standard Time");
        }

        public static string FormattedDayMonthYear(this DateTime dateTime)
        {
            return string.Format("{0:dd/MM/yyyy}", dateTime);
        }

        public static string FormattedDayMonthYearAndHours(this DateTime dateTime)
        {
            return string.Format("{0:dd/MM/yyyy} às {0:HH:mm}", dateTime);
        }

        public static bool IsValid(this DateTime dateTime)
        {
            return dateTime != DateTime.MinValue && dateTime != DateTime.MaxValue;
        }
    }
}
