﻿using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class AtividadesExtrasController : Controller
    {
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var atividades = new ExtraActivityService().ListarAtividades(User.Identity.Name, idCurriculo);
            return View(atividades);
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo()
        {
            ViewBag.tiposAtividade = new EnumService().ListarTiposAtividade().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            return View("Novo");
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo(ExtraActivity atividade, int idCurriculo)
        {
            var model = new ExtraActivityService().Novo(User.Identity.Name, idCurriculo, atividade.NameCityCountry, atividade.City, atividade.EnumTypeActivity, atividade.InitialDate, atividade.EndDate, atividade.EnumCountry, atividade.Description);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.AtividadesExtras.Novo());
            }

            TempData["Alerta"] = new Alert("success", "Atividade criada com sucesso.");
            return RedirectToAction(MVC.AtividadesExtras.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idAtividade)
        {
            ViewBag.tiposAtividade = new EnumService().ListarTiposAtividade().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            var atividade = new ExtraActivityService().DetalhesAtividade(User.Identity.Name, idAtividade);
            ViewBag.cidades = new EnumService().ListarCidades(atividade.State).ToSelectList(x => x, x => x).SelectValues();
            return View(atividade);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(ExtraActivity atividade, int idCurriculo)
        {
            var model = new ExtraActivityService().Editar(User.Identity.Name, atividade.Id, atividade.NameCityCountry, atividade.City, atividade.EnumTypeActivity, atividade.InitialDate, atividade.EndDate, atividade.EnumCountry, atividade.Description);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.AtividadesExtras.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Atividade excluída com sucesso.");
            return RedirectToAction(MVC.AtividadesExtras.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idAtividade)
        {
            var atividade = new ExtraActivityService().DetalhesAtividade(User.Identity.Name, idAtividade);
            return PartialView("_Excluir",atividade);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(ExtraActivity atividade, int idCurriculo)
        {
            var model = new ExtraActivityService().Excluir(User.Identity.Name, atividade.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.AtividadesExtras.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Atividade excluída com sucesso.");
            return RedirectToAction(MVC.AtividadesExtras.Index(idCurriculo));
        }
    }
}