﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class ProcessosSeletivosController : Controller
    {
        [RequiresAuthorization, LoadData]
        public virtual ActionResult Index(int idEtapa)
        {
            var processos = new SelectiveProcessService().ListarProcessosEtapa(User.Identity.Name, idEtapa);
            return View(processos);
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult ExcluirProcessoSeletivo(int idProcesso)
        {
            var processo = new SelectiveProcessService().Detalhes(User.Identity.Name, idProcesso);
            return PartialView("_Excluir", processo);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult ExcluirProcessoSeletivo(SelectiveProcess processo)
        {
            var model = new SelectiveProcessService().Excluir(User.Identity.Name, processo.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.ProcessosSeletivos.Index(processo.Id));
            }

            TempData["Alerta"] = new Alert("success", "Certificação criada com sucesso.");
            return RedirectToAction(MVC.ProcessosSeletivos.Index(processo.Id));
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult VagasProcesso()
        {
            var vagas = new VacancyService().ListarVagasEmpresa(User.Identity.Name, AuthorizationToken.Token);
            return View(vagas);
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult Gerenciar(int idVaga)
        {
            var vaga = new VacancyService().Detalhes(User.Identity.Name, idVaga);
            ViewBag.idVaga = vaga.Id;
            ViewBag.etapaAtual = new VacancyService().DetalhesEtapa(User.Identity.Name, vaga.IdCurrentStep);
            ViewBag.Etapas = new VacancyService().ListarEtapas(User.Identity.Name, vaga.Id).Steps.Where(x=>x.EnumStatus == "Pendente").ToSelectList(x=>x.Id,x=>x.EnumStep).SelectValues();
            var processos = new SelectiveProcessService().ListarProcessosEtapa(User.Identity.Name, vaga.IdCurrentStep); 
            return View(processos);
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult AprovarCandidatos(List<string> aprovados, int? idProximaEtapa, bool enviarEmail, int idEtapaAtual, int idVaga, string mensagemAprovados, string mensagemReprovados)
        {
            var processo = new SelectiveProcessService().AprovarCandidatos(User.Identity.Name, idEtapaAtual, idProximaEtapa, aprovados, enviarEmail, mensagemAprovados, mensagemReprovados);
            if (!string.IsNullOrWhiteSpace(processo.Error))
            {
                TempData["Alerta"] = new Alert("error", processo.Error);
                return RedirectToAction(MVC.ProcessosSeletivos.Gerenciar(idVaga));
            }

            TempData["Alerta"] = new Alert("success", "Certificação criada com sucesso.");
            return RedirectToAction(MVC.ProcessosSeletivos.Gerenciar(idVaga));

        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult Observacao(int idProcesso)
        {
            var processo = new SelectiveProcessService().Detalhes(User.Identity.Name, idProcesso);
            return PartialView("_observacao", processo);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Observacao(int idProcesso, string observacao)
        {
            var vaga = new SelectiveProcessService().Observacao(User.Identity.Name, idProcesso, observacao);
            if (!string.IsNullOrWhiteSpace(vaga.Error))
            {
                TempData["Alerta"] = new Alert("error", vaga.Error);
                return RedirectToAction(MVC.ProcessosSeletivos.Gerenciar(vaga.Id));
            }

            TempData["Alerta"] = new Alert("success", "Observação alterada com sucesso.");
            return RedirectToAction(MVC.ProcessosSeletivos.Gerenciar(vaga.Id));
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult AdicionarCandidatoEtapa(int idEtapa)
        {
            ViewBag.idEtapa = idEtapa;
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult AdicionarCandidatoEtapa(int idEtapa, int idCurriculo)
        {
            var processo = new SelectiveProcessService().InserirCandidatoEtapa(User.Identity.Name, idCurriculo, idEtapa);
            if (!string.IsNullOrWhiteSpace(processo.Error))
            {
                TempData["Alerta"] = new Alert("error", processo.Error);
                return RedirectToAction(MVC.ProcessosSeletivos.Index(idEtapa));
            }

            TempData["Alerta"] = new Alert("success", "Candidato adicionado com sucesso.");
            return RedirectToAction(MVC.ProcessosSeletivos.Index(idEtapa));
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult VagasHistorico()
        {
            var vagas = new VacancyService().ListarVagasEmpresa(User.Identity.Name, AuthorizationToken.Token);
            return View(vagas);
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult Historico(int idEtapa, int idVaga)
        {
            var processos = new SelectiveProcessService().ListarProcessosEtapa(User.Identity.Name, idEtapa);
            ViewBag.etapas = new VacancyService().ListarEtapas(User.Identity.Name, idVaga).Steps.ToSelectList(x => x.Id, x => x.EnumStep).SelectValues();
            ViewBag.etapa = new VacancyService().DetalhesEtapa(User.Identity.Name, idEtapa);
            return View(processos);
        }
    }
}