﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class HomeController : Controller
    {
        [LoadData]
        public virtual ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction(MVC.Usuarios.Index());
            }

            var vagas = new VacancyService().ListarVagasDisponiveis(AuthorizationToken.Token, User.Identity.Name);
            return View(vagas);
        }

        public virtual ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public virtual ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}