﻿using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class IdiomasController : Controller
    {
        // GET: Idiomas
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var idiomas = new KnowledgeLanguageService().ListarIdiomas(User.Identity.Name, idCurriculo);
            return View(idiomas);
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo()
        {
            ViewBag.niveis = new EnumService().ListarNivelIdioma().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.idiomas = new EnumService().ListarIdiomas().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo(KnowledgeLanguage idioma, int idCurriculo)
        {
            var model = new KnowledgeLanguageService().Novo(User.Identity.Name, idCurriculo, idioma.EnumLanguage, idioma.EnumConversation, idioma.EnumReading, idioma.EnumWriting);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Idiomas.Novo());
            }
            TempData["Alerta"] = new Alert("success", "Idioma criado com sucesso.");
            return RedirectToAction(MVC.Idiomas.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idIdioma)
        {
            ViewBag.niveis = new EnumService().ListarNivelIdioma().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.idiomas = new EnumService().ListarIdiomas().ToSelectList(x => x, x => x).SelectValues();
            var idioma = new KnowledgeLanguageService().Detalhes(User.Identity.Name, idIdioma);
            return View(idioma);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(KnowledgeLanguage idioma, int idCurriculo)
        {
            var model = new KnowledgeLanguageService().Editar(User.Identity.Name, idioma.Id, idioma.EnumLanguage, idioma.EnumConversation, idioma.EnumReading, idioma.EnumWriting);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Idiomas.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Idioma alterado com sucesso.");
            return RedirectToAction(MVC.Idiomas.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idIdioma)
        {
            var idioma = new KnowledgeLanguageService().Detalhes(User.Identity.Name, idIdioma);
            return PartialView("_Excluir", idioma);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(KnowledgeLanguage idioma, int idCurriculo)
        {
            var model = new KnowledgeLanguageService().Excluir(User.Identity.Name, idioma.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Idiomas.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Idioma excluído com sucesso.");
            return RedirectToAction(MVC.Idiomas.Index(idCurriculo));
        }
    }
}