﻿using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class CursosController : Controller
    {
        // GET: Cursos
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var cursos = new ProfessionalCourseService().ListarCursos(User.Identity.Name, idCurriculo);
            return View(cursos);
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo()
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo(ProfessionalCourse curso, int idCurriculo)
        {
            var model = new ProfessionalCourseService().Novo(User.Identity.Name, idCurriculo, curso.City, curso.NameCityCountry, curso.Institute, curso.ConclusionDate, curso.EnumCountry, curso.Course, curso.Workload);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Cursos.Novo());
            }

            TempData["Alerta"] = new Alert("success", "Curso criado com sucesso.");
            return RedirectToAction(MVC.Cursos.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idCurso)
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            var curso = new ProfessionalCourseService().Detalhes(User.Identity.Name, idCurso);
            ViewBag.cidades = new EnumService().ListarCidades(curso.State).ToSelectList(x => x, x => x).SelectValues();
            return View(curso);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(ProfessionalCourse curso, int idCurriculo)
        {
            var model = new ProfessionalCourseService().Editar(User.Identity.Name, curso.Id, curso.City, curso.NameCityCountry, curso.Institute, curso.ConclusionDate, curso.EnumCountry, curso.Course, curso.Workload);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Cursos.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Curso alterado com sucesso.");
            return RedirectToAction(MVC.Cursos.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idCurso)
        {
            var curso = new ProfessionalCourseService().Detalhes(User.Identity.Name, idCurso);
            return PartialView("_Excluir", curso);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(ProfessionalCourse curso, int idCurriculo)
        {
            var model = new ProfessionalCourseService().Excluir(User.Identity.Name, curso.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Cursos.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Curso excluído com sucesso.");
            return RedirectToAction(MVC.Cursos.Index(idCurriculo));
        }
    }
}