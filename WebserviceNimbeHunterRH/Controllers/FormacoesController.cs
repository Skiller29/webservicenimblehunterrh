﻿using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class FormacoesController : Controller
    {
        // GET: Formacoes
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var formacoes = new AcademicFormationService().ListarFormacoes(User.Identity.Name, idCurriculo);
            return View(formacoes);
        }

        [HttpGet, LoadData]
        public virtual ActionResult Novo()
        {
            ViewBag.tiposFormacao = new EnumService().ListarTiposFormacao().ToSelectList(x => x, x => x).SelectValues(); ;
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.status = new EnumService().ListarStatusFormacao().ToSelectList(x => x, x => x).SelectValues(); ;
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo(AcademicFormation formacao, int idCurriculo)
        {
            var model = new AcademicFormationService().Novo(User.Identity.Name, idCurriculo, formacao.NameCityCountry, formacao.City, formacao.Institute, formacao.StatusFormation, formacao.TypeFormation, formacao.InitialDate, formacao.EndDate, formacao.Country, formacao.Course);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Formacoes.Novo());
            };
            TempData["Alerta"] = new Alert("success", "Formãção criada com sucesso.");
            return RedirectToAction(MVC.Formacoes.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idFormacao)
        {
            ViewBag.tiposFormacao = new EnumService().ListarTiposFormacao().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.status = new EnumService().ListarStatusFormacao().ToSelectList(x => x, x => x).SelectValues();
            var formacao = new AcademicFormationService().Detalhes(User.Identity.Name, idFormacao);
            ViewBag.cidades = new EnumService().ListarCidades(formacao.State).ToSelectList(x => x, x => x).SelectValues();
            return View(formacao);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(AcademicFormation formacao, int idCurriculo)
        {
            var model = new AcademicFormationService().Editar(User.Identity.Name, formacao.Id, formacao.NameCityCountry, formacao.City, formacao.Institute, formacao.StatusFormation, formacao.TypeFormation, formacao.InitialDate, formacao.EndDate, formacao.Country, formacao.Course);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Formacoes.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Formação alterada com sucesso.");
            return RedirectToAction(MVC.Formacoes.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idFormacao)
        {
            var formacao = new AcademicFormationService().Detalhes(User.Identity.Name, idFormacao);
            return PartialView("_Excluir", formacao);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(AcademicFormation formacao, int idCurriculo)
        {
            var model = new AcademicFormationService().Excluir(User.Identity.Name, formacao.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Formacoes.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Formação excluída com sucesso.");
            return RedirectToAction(MVC.Formacoes.Index(idCurriculo));
        }
    }
}