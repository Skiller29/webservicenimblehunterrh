﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class CamposAdicionaisController : Controller
    {
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idVaga)
        {
            var campos = new AdditionalFieldService().ListarCamposVaga(User.Identity.Name, idVaga);
            ViewBag.idVaga = idVaga;
            return View(campos);
        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult Novo(int idVaga)
        {
            ViewBag.idVaga = idVaga;
            ViewBag.tiposCampo = new EnumService().ListarTiposCampoAdicional().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult Novo(AdditionalField campo, int idVaga)
        {
            var model = new AdditionalFieldService().Novo(User.Identity.Name, idVaga, campo.NameField, campo.EnumTypeField);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.CamposAdicionais.Novo());
            }

            TempData["Alerta"] = new Alert("success", "Campo criado com sucesso.");
            return RedirectToAction(MVC.CamposAdicionais.Index(idVaga));
        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult Editar(int idCampo)
        {
            ViewBag.tiposCampo = new EnumService().ListarTiposCampoAdicional().ToSelectList(x => x, x => x).SelectValues();
            var campo = new AdditionalFieldService().Detalhes(User.Identity.Name, idCampo);
            return View(campo);
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult Editar(AdditionalField campo, int idVaga)
        {
            var model = new AdditionalFieldService().Editar(User.Identity.Name, campo.Id, campo.NameField, campo.EnumTypeField);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.CamposAdicionais.Editar(campo.Id));
            }

            TempData["Alerta"] = new Alert("success", "Campo alterado com sucesso.");
            return RedirectToAction(MVC.CamposAdicionais.Index(idVaga));
        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult Excluir(int idCampo)
        {
            var campo = new AdditionalFieldService().Detalhes(User.Identity.Name, idCampo);
            return PartialView("_Excluir",campo);
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult Excluir(AdditionalField campo, int idVaga)
        {
            var model = new AdditionalFieldService().Excluir(User.Identity.Name, campo.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.CamposAdicionais.Index(idVaga));
            }

            TempData["Alerta"] = new Alert("success", "Campo excluído com sucesso.");
            return RedirectToAction(MVC.CamposAdicionais.Index(idVaga));
        }
    }
}