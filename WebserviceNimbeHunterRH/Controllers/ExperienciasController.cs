﻿using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class ExperienciasController : Controller
    {
        // GET: Experiencias
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var experiencias = new ProfessionalExperienceService().ListarExperienciasProfissionais(User.Identity.Name,
                idCurriculo);
            return View(experiencias);
        }

        [HttpGet, LoadData]
        public virtual ActionResult Novo()
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.areas = new EnumService().ListarAreasOcupacao().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo(ProfessionalExperience experiencia, int idCurriculo)
        {
            var model = new ProfessionalExperienceService().Novo(User.Identity.Name, idCurriculo, experiencia.NameCityCountry, experiencia.City, experiencia.EnumCountry, experiencia.Company, experiencia.CompanyPhone, experiencia.ActivitiesPerformed, experiencia.EnumOccupationArea, experiencia.LastSalary, experiencia.TerminationReason, experiencia.AdmissionDate, experiencia.TerminationDate, experiencia.Occupation, experiencia.ImmediateSuperior);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Experiencias.Novo());
            }
            TempData["Alerta"] = new Alert("success", "Experiência criada com sucesso.");
            return RedirectToAction(MVC.Experiencias.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idExperiencia)
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.areas = new EnumService().ListarAreasOcupacao().ToSelectList(x => x, x => x).SelectValues();
            var experiencia = new ProfessionalExperienceService().Detalhes(User.Identity.Name, idExperiencia);
            ViewBag.cidades = new EnumService().ListarCidades(experiencia.State).ToSelectList(x => x, x => x).SelectValues();
            return View(experiencia);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(ProfessionalExperience experiencia, int idCurriculo)
        {
            var model = new ProfessionalExperienceService().Editar(User.Identity.Name, experiencia.Id, experiencia.NameCityCountry, experiencia.City, experiencia.EnumCountry, experiencia.Company, experiencia.CompanyPhone, experiencia.ActivitiesPerformed, experiencia.EnumOccupationArea, experiencia.LastSalary, experiencia.TerminationReason, experiencia.AdmissionDate, experiencia.TerminationDate, experiencia.Occupation, experiencia.ImmediateSuperior);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Experiencias.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Experiência alterada com sucesso.");
            return RedirectToAction(MVC.Experiencias.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idExperiencia)
        {
            var experiencia = new ProfessionalExperienceService().Detalhes(User.Identity.Name, idExperiencia);
            return PartialView("_Excluir", experiencia);

        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(ProfessionalExperience experiencia, int idCurriculo)
        {
            var model = new ProfessionalExperienceService().Excluir(User.Identity.Name, experiencia.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Experiencias.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Experiência excluída com sucesso.");
            return RedirectToAction(MVC.Experiencias.Index(idCurriculo));
        }
    }
}