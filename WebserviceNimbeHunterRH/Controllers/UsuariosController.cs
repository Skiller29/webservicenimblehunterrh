﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class UsuariosController : Controller
    {
        [LoadData,RequiresAuthorization]
        public virtual ActionResult Index()
        {
            var usuario = new UserService().Detalhes(User.Identity.Name);
            return View(usuario);
        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult PaginaInicial()
        {
            return View();
        }

        [HttpPost, LoadData]
        public virtual ActionResult Login(string cpf, string senha)
        {
            var usuario = new UserService().Login(cpf, senha, AuthorizationToken.Token);
            if (!string.IsNullOrEmpty(usuario.Error))
            {
                TempData["Alerta"] = new Alert("error", "Login ou senha inválidos.");
                return RedirectToAction(MVC.Home.Index());
            }

            TempData["Alerta"] = new Alert("success", "Bem vindo "+ usuario.FullName + ".");
            FormsAuthentication.SetAuthCookie(usuario.Token, true);
            if (usuario.EnumProfile.Equals("Candidato"))
                return RedirectToAction(MVC.Usuarios.Index());

                return RedirectToAction(MVC.Usuarios.PaginaInicial());   
        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction(MVC.Home.Index());
        }

        [HttpPost, LoadData]
        public virtual ActionResult Novo(string nome, string cpf, string senha)
        {
            var usuario = new UserService().Novo(cpf, senha, nome, AuthorizationToken.Token);
            if(!string.IsNullOrEmpty(usuario.Error)) return RedirectToAction(MVC.Home.Index());

            FormsAuthentication.SetAuthCookie(usuario.Token, true);
            return RedirectToAction(MVC.Usuarios.Index());
        }
       
        [RequiresAuthorization, LoadData]
        public virtual ActionResult EditarDadosPessoais()
        {
            var usuario = new UserService().Detalhes(User.Identity.Name);
            ViewBag.generos = new EnumService().ListarGeneros().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.cidades = new EnumService().ListarCidades(usuario.State).ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estadoCivil = new EnumService().ListarEstadoscivis().ToSelectList(x => x, x => x).SelectValues();
            return View(usuario);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult EditarDadosPessoais(User usuario)
        {
            var model = new UserService().EditarDadosPessoais(User.Identity.Name, usuario.City, usuario.FullName,
                usuario.BirthDate, usuario.Nationality, usuario.EnumGenre, usuario.EnumMaritalStatus,
                usuario.IsDeficient, usuario.Address, usuario.Cep, usuario.Number, usuario.Complement,
                usuario.Neighborhood, usuario.Email, usuario.HomePhone, usuario.CellPhone, usuario.OptionalPhone);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Usuarios.Index());
            }

            TempData["Alerta"] = new Alert("success", "Dados alterados com sucesso.");
            return RedirectToAction(MVC.Usuarios.Index());

        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult AlterarFotoUsuario()
        {
            return PartialView("_alterarFotoUsuario");
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult AlterarFotoUsuario(HttpPostedFileBase arquivo)
        {
            var usuario = new UserService().AlterarFotoUsuario(User.Identity.Name, arquivo);
            if (!string.IsNullOrWhiteSpace(usuario.Error))
            {
                TempData["Alerta"] = new Alert("error", usuario.Error);
                return RedirectToAction(MVC.Usuarios.Index());
            }

            TempData["Alerta"] = new Alert("success", "Dados alterados com sucesso.");
            return RedirectToAction(MVC.Usuarios.Index());
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult AlterarSenha()
        {
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult AlterarSenha(string senhaAntiga, string novaSenha, string repetirSenha)
        {
            var usuario = new UserService().AlterarSenha(User.Identity.Name, senhaAntiga, novaSenha, AuthorizationToken.Token);
            if (!string.IsNullOrWhiteSpace(usuario.Error))
            {
                TempData["Alerta"] = new Alert("error", usuario.Error);
                return RedirectToAction(MVC.Usuarios.AlterarSenha());
            }

            TempData["Alerta"] = new Alert("success", "Senha alterada com sucesso.");
            return RedirectToAction(MVC.Usuarios.Index());
        }

        [RequiresAuthorization, LoadData]
        public virtual ActionResult ProcessosSeletivos()
        {
            var processos = new UserService().MeusProcessosSeletivos(User.Identity.Name);
            return View(processos);
        }
    }
}