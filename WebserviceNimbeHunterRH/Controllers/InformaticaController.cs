﻿using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class InformaticaController : Controller
    {
        // GET: Informatica
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var informatica = new KnowledgeInformaticService().ListarInformatica(User.Identity.Name, idCurriculo);
            return View(informatica);
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo()
        {
            ViewBag.niveis = new EnumService().ListarNiveisInformatica().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Novo(KnowledgeInformatic informatica, int idCurriculo)
        {
            var model = new KnowledgeInformaticService().Novo(User.Identity.Name, idCurriculo, informatica.Software,informatica.EnumLevel);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Informatica.Novo());
            }

            TempData["Alerta"] = new Alert("success", "Habilidade criada com sucesso.");
            return RedirectToAction(MVC.Informatica.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idInformatica)
        {
            ViewBag.niveis = new EnumService().ListarNiveisInformatica().ToSelectList(x => x, x => x).SelectValues();
            var informatica = new KnowledgeInformaticService().DetalhesInformatica(User.Identity.Name, idInformatica);
            return View(informatica);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(KnowledgeInformatic informatica, int idCurriculo)
        {
            var model = new KnowledgeInformaticService().Editar(User.Identity.Name, informatica.Id, informatica.Software, informatica.EnumLevel);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Informatica.Index(idCurriculo));
            }
            TempData["Alerta"] = new Alert("success", "Habilidade alterada com sucesso.");
            return RedirectToAction(MVC.Informatica.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idInformatica)
        {
            var informatica = new KnowledgeInformaticService().DetalhesInformatica(User.Identity.Name, idInformatica);
            return PartialView("_Excluir", informatica);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(KnowledgeInformatic informatica, int idCurriculo)
        {
            var model = new KnowledgeInformaticService().Excluir(User.Identity.Name, informatica.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Informatica.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Habilidade excluída com sucesso.");
            return RedirectToAction(MVC.Informatica.Index(idCurriculo));
        }
    }
}