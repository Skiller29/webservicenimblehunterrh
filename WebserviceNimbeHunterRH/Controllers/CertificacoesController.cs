﻿using System.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class CertificacoesController : Controller
    {
        // GET: Certificacoes
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var certificacoes = new CertificateService().ListarCertificacoes(User.Identity.Name, idCurriculo);
            return View(certificacoes);
        }

        [HttpGet, LoadData]
        public virtual ActionResult Novo()
        {
            return View();
        }

        [HttpPost, LoadData]
        public virtual ActionResult Novo(Certificate certificado, int idCurriculo)
        {
            var model = new CertificateService().Novo(User.Identity.Name, idCurriculo, certificado.Certification, certificado.NumberOrganClass, certificado.OrganIssuer);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Certificacoes.Novo());
            }

            TempData["Alerta"] = new Alert("success", "Certificação criada com sucesso.");
            return RedirectToAction(MVC.Certificacoes.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(int idCertificado)
        {
            var certificacao = new CertificateService().DetalhesCertificado(User.Identity.Name, idCertificado);
            return View(certificacao);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Editar(Certificate certificado, int idCurriculo)
        {
            var model = new CertificateService().Editar(User.Identity.Name, certificado.Id, certificado.Certification, certificado.NumberOrganClass, certificado.OrganIssuer);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Certificacoes.Index(idCurriculo));
            }
            TempData["Alerta"] = new Alert("success", "Certificação alterada com sucesso.");
            return RedirectToAction(MVC.Certificacoes.Index(idCurriculo));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(int idCertificado)
        {
            var certificacao = new CertificateService().DetalhesCertificado(User.Identity.Name, idCertificado);
            return PartialView("_Excluir", certificacao);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult Excluir(ExtraActivity certificado, int idCurriculo)
        {
            var model = new CertificateService().Excluir(User.Identity.Name, certificado.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Certificacoes.Index(idCurriculo));
            }
            TempData["Alerta"] = new Alert("success", "Certificação excluída com sucesso.");
            return RedirectToAction(MVC.Certificacoes.Index(idCurriculo));
        }
    }
}