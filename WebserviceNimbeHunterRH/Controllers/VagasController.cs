﻿using System.Collections.Generic;
using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class VagasController : Controller
    {
        // GET: Vagas
        [RequiresAuthorization, LoadData]
        public virtual ActionResult Index()
        {
            var usuario = new UserService().Detalhes(User.Identity.Name);
            Vacancies vagas = new Vacancies();
            if (usuario.EnumProfile == "Candidato")
            {
                vagas = new VacancyService().ListarVagasDisponiveis(AuthorizationToken.Token, User.Identity.Name);
            }
            else
            {
                vagas = new VacancyService().ListarVagasEmpresa(User.Identity.Name, AuthorizationToken.Token);
            }            
            return View(vagas);
        }

        [LoadData]
        public virtual ActionResult DetalhesVaga(int idVaga)
        {
            var vaga = new VacancyService().Detalhes(User.Identity.Name, idVaga);
            return PartialView("_detalhes-vaga", vaga);
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult NovaVaga()
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult NovaVaga(Vacancy vaga)
        {
            var model = new VacancyService().Novo(User.Identity.Name, vaga.City, vaga.Ocupation, vaga.IsVacancyDeficient,
                vaga.IsVisibleCandidate, vaga.Description, AuthorizationToken.Token);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Vagas.NovaVaga());
            }

            TempData["Alerta"] = new Alert("success", "Vaga criada com sucesso.");
            return RedirectToAction(MVC.Vagas.Index());
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult EditarVaga(int idVaga)
        {
            var vaga = new VacancyService().Detalhes(User.Identity.Name, idVaga);
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.cidades = new EnumService().ListarCidades(vaga.State).ToSelectList(x => x, x => x).SelectValues();
            return View(vaga);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult EditarVaga(Vacancy vaga)
        {
            var model = new VacancyService().Editar(User.Identity.Name, vaga.City, vaga.Ocupation, vaga.IsVacancyDeficient, vaga.IsVisibleCandidate, vaga.Description, vaga.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Vagas.EditarVaga());
            }

            TempData["Alerta"] = new Alert("success", "Vaga alterada com sucesso.");
            return RedirectToAction(MVC.Vagas.Index());
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult ExcluirVaga(int idVaga)
        {
            var vaga = new VacancyService().Detalhes(User.Identity.Name, idVaga);
            return PartialView("_Excluir-vaga",vaga);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult ExcluirVaga(Vacancy vaga)
        {
            var model = new VacancyService().Excluir(User.Identity.Name, vaga.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Vagas.Index());
            }

            TempData["Alerta"] = new Alert("success", "Vaga excluída com sucesso.");
            return RedirectToAction(MVC.Vagas.Index());
        }


        //ETAPAS
        [RequiresAuthorization, LoadData]
        public virtual ActionResult EtapasVaga(int idVaga)
        {
            var etapas = new VacancyService().ListarEtapas(User.Identity.Name, idVaga);
            ViewBag.idVaga = idVaga;
            return View(etapas);
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult NovaEtapaVaga(int idVaga)
        {
            ViewBag.idVaga = idVaga;
            ViewBag.etapasVaga = new EnumService().ListarEtapasVagas().ToSelectList(x => x, x => x).SelectValues();
            return View("NovaEtapa");
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult NovaEtapaVaga(Stepvacancy etapa, int idVaga)
        {
            var model = new VacancyService().NovaEtapa(User.Identity.Name, idVaga, etapa.EnumStep, etapa.InitialDate, etapa.EndTime);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Vagas.EtapasVaga(idVaga));
            }

            TempData["Alerta"] = new Alert("success", "Etapa criada com sucesso.");
            return RedirectToAction(MVC.Vagas.EtapasVaga(idVaga));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult EditarEtapaVaga(int idEtapa)
        {
            ViewBag.etapasVaga = new EnumService().ListarEtapasVagas().ToSelectList(x => x, x => x).SelectValues();
            var etapa = new VacancyService().DetalhesEtapa(User.Identity.Name, idEtapa);
            return View("EditarEtapa",etapa);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult EditarEtapaVaga(Stepvacancy etapa)
        {
            var model = new VacancyService().EditarEtapa(User.Identity.Name, etapa.Id, etapa.EnumStep, etapa.InitialDate, etapa.EndTime);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Vagas.EditarEtapaVaga(etapa.Id));
            }

            TempData["Alerta"] = new Alert("success", "Etapa alterada com sucesso.");
            return RedirectToAction(MVC.Vagas.EtapasVaga(model.IdVacancy));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult ExcluirEtapaVaga(int idEtapa)
        {
            var etapa = new VacancyService().DetalhesEtapa(User.Identity.Name, idEtapa);
            return PartialView("_Excluir-etapa",etapa);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult ExcluirEtapaVaga(Stepvacancy etapa, int idVaga)
        {
            var model = new VacancyService().ExcluirEtapa(User.Identity.Name, etapa.Id);
            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Vagas.EtapasVaga(idVaga));
            }

            TempData["Alerta"] = new Alert("success", "Etapa excluída com sucesso.");
            return RedirectToAction(MVC.Vagas.EtapasVaga(idVaga));
        }

        [HttpGet, RequiresAuthorization, LoadData]
        public virtual ActionResult CandidatarVaga(int idVaga)
        {
            var usuario = new UserService().Detalhes(User.Identity.Name);
            if (string.IsNullOrWhiteSpace(usuario.Email))
            {
                TempData["Alerta"] = new Alert("error", "Para se candidatar a uma vaga é necessário ter um email cadastrado.");
                return RedirectToAction(MVC.Vagas.Index());
            }

            var vaga = new VacancyService().Detalhes(User.Identity.Name, idVaga);
            if (vaga.HasFields)
            {
                var campos = new AdditionalFieldService().ListarCamposVaga(User.Identity.Name, vaga.Id);
                ViewBag.campos = campos;
            }
            return View("ConfirmacaoCandidatura", vaga);
        }

        [HttpPost, RequiresAuthorization, LoadData]
        public virtual ActionResult CandidatarVaga(int idVaga, List<string> respostas)
        {
            var processo = new VacancyService().CandidatarVaga(User.Identity.Name, idVaga, respostas);
            if (!string.IsNullOrWhiteSpace(processo.Error))
            {
                TempData["Alerta"] = new Alert("error", processo.Error);
                return RedirectToAction(MVC.Vagas.CandidatarVaga(idVaga));
            }

            TempData["Alerta"] = new Alert("success", "Você foi cadastrado no processo seletivo da vaga com sucesso.");
            return RedirectToAction(MVC.Usuarios.ProcessosSeletivos());
        }

    }
}