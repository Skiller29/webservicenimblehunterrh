﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class CurriculosController : Controller
    {
        // GET: Curriculum
        [LoadData, RequiresAuthorization]
        public virtual ActionResult Index(int idCurriculo)
        {
            var curriculo = new CurriculumService().Detalhes(User.Identity.Name, idCurriculo);
            return View(curriculo);
        }

        [HttpGet, LoadData, RequiresAuthorization]
        public virtual ActionResult Novo()
        {
            ViewBag.areas = new EnumService().ListarAreasInteresse().ToSelectList(x=>x, x=>x).SelectValues();
            ViewBag.grauparentesco = new EnumService().ListarGrausParentesco().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.salarios = new EnumService().ListarSalarios().ToSelectList(x => x, x => x).SelectValues();
            return View();
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult Novo(Curriculum model)
        {
            var curriculo = new CurriculumService().Novo(User.Identity.Name, model.WorkedCompany, model.SectorWorked,
                model.AdmitionDateWork, model.TerminationDateWork, model.TerminationReasonWork, model.HasKinshipWorked,
                model.EnumDegreeKinship, model.NameKinship, model.SectorKinship, model.PhoneKinship,
                model.EnumFirstAreaInterest, model.EnumSecondAreaInterest, model.EnumThirdAreaInterest,
                model.EnumSalaryRequeriments);

            if (!string.IsNullOrWhiteSpace(curriculo.Error))
            {
                TempData["Alerta"] = new Alert("error", curriculo.Error);
                return RedirectToAction(MVC.Curriculos.Novo());
            }

            TempData["Alerta"] = new Alert("success", "Currículo criado com sucesso.");
            return RedirectToAction(MVC.Usuarios.Index());
        }

        [HttpGet, LoadData, RequiresAuthorization]
        public virtual ActionResult Editar(int idCurriculo)
        {
            ViewBag.areas = new EnumService().ListarAreasInteresse().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.grauparentesco = new EnumService().ListarGrausParentesco().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.salarios = new EnumService().ListarSalarios().ToSelectList(x => x, x => x).SelectValues();
            var curriculo = new CurriculumService().Detalhes(User.Identity.Name, idCurriculo);
            return View(curriculo);
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult Editar(Curriculum curriculo, int idCurriculo)
        {
            var model = new CurriculumService().Editar(User.Identity.Name, curriculo.Id, curriculo.WorkedCompany,
                curriculo.SectorWorked, curriculo.AdmitionDateWork, curriculo.TerminationDateWork,
                curriculo.TerminationReasonWork, curriculo.HasKinshipWorked, curriculo.EnumDegreeKinship,
                curriculo.NameKinship, curriculo.SectorKinship, curriculo.PhoneKinship, curriculo.EnumFirstAreaInterest,
                curriculo.EnumSecondAreaInterest, curriculo.EnumThirdAreaInterest, curriculo.EnumSalaryRequeriments);

            if (!string.IsNullOrWhiteSpace(model.Error))
            {
                TempData["Alerta"] = new Alert("error", model.Error);
                return RedirectToAction(MVC.Curriculos.Index(idCurriculo));
            }

            TempData["Alerta"] = new Alert("success", "Currículo alterado com sucesso.");
            return RedirectToAction(MVC.Curriculos.Index(idCurriculo));

        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult VerCurriculoCompleto(int idUsuario)
        {
            var usuario = new UserService().ProcurarUsuario(User.Identity.Name, idUsuario);
            var formacoes = new AcademicFormationService().ListarFormacoes(User.Identity.Name, usuario.IdCurriculum);
            var experiencias = new ProfessionalExperienceService().ListarExperienciasProfissionais(User.Identity.Name, usuario.IdCurriculum);
            var informatica = new KnowledgeInformaticService().ListarInformatica(User.Identity.Name, usuario.IdCurriculum);
            var idiomas = new KnowledgeLanguageService().ListarIdiomas(User.Identity.Name, usuario.IdCurriculum);
            var atividades = new ExtraActivityService().ListarAtividades(User.Identity.Name, usuario.IdCurriculum);
            var cursos = new ProfessionalCourseService().ListarCursos(User.Identity.Name, usuario.IdCurriculum);
            var curriculo = new CurriculumService().Detalhes(User.Identity.Name, usuario.IdCurriculum);
            var certificados = new CertificateService().ListarCertificacoes(User.Identity.Name, usuario.IdCurriculum);
            var curriculoCompleto = new CompleteCurriculum(usuario, formacoes.Formations, cursos.Courses, experiencias.Experiences, atividades.Activites, certificados.CertificatesList, informatica.Informatics, idiomas.Languages, curriculo);
            return View("_curriculo-completo", curriculoCompleto);
        }
    }
}