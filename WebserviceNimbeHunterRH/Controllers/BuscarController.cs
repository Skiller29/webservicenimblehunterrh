﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Simple.Web.Mvc;
using WebserviceNimbeHunterRH.Helpers;
using WebserviceNimbleHunterLibraries.Models;
using WebserviceNimbleHunterLibraries.Services;

namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class BuscarController : Controller
    {
        // GET: Buscar
        public virtual ActionResult Index()
        {
            return View();
        }


        [LoadData, RequiresAuthorization]
        public virtual ActionResult BuscarPorFormacaoAcademica()
        {
            ViewBag.tiposFormacao = new EnumService().ListarTiposFormacao().ToSelectList(x => x, x => x).SelectValues(); ;
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.status = new EnumService().ListarStatusFormacao().ToSelectList(x => x, x => x).SelectValues(); ;
            return View("BuscarPorFormacaoAcademica");
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult BuscarPorFormacaoAcademica(AcademicFormation formacao)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorFormacaoAcademica(User.Identity.Name,
                formacao.StatusFormation, formacao.TypeFormation, formacao.Course, formacao.Institute, formacao.Country,
                formacao.NameCityCountry, formacao.City);

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-experiencia-profissional")]
        public virtual ActionResult BuscarPorExperienciaProfissional()
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.areas = new EnumService().ListarAreasOcupacao().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorExperienciaProfissional");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-experiencia-profissional")]
        public virtual ActionResult BuscarPorExperienciaProfissional(ProfessionalExperience experiencia)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorExperienciaProfissional(User.Identity.Name,
                    experiencia.Company, experiencia.Occupation, experiencia.EnumOccupationArea, experiencia.EnumCountry,
                    experiencia.NameCityCountry, experiencia.City);
            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization]
        public virtual ActionResult BuscarPorCertificacao()
        {
            return View("BuscarPorCertificacao");
        }

        [HttpPost, LoadData, RequiresAuthorization]
        public virtual ActionResult BuscarPorCertificacao(Certificate certificacao)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorCertificacao(User.Identity.Name,
                certificacao.Certification, certificacao.OrganIssuer, certificacao.NumberOrganClass.ToString());

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-dados-pessoais")]
        public virtual ActionResult BuscarPorDadosPessoais()
        {
            ViewBag.generos = new EnumService().ListarGeneros().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estadoCivil = new EnumService().ListarEstadoscivis().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorDadosPessoais");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-dados-pessoais")]
        public virtual ActionResult BuscarPorDadosPessoais(User usuario)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorDadosPessoais(User.Identity.Name, usuario.FullName, usuario.EnumGenre, usuario.EnumMaritalStatus, usuario.City, usuario.Neighborhood);
            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-informatica")]
        public virtual ActionResult BuscarPorInformatica()
        {
            ViewBag.niveis = new EnumService().ListarNiveisInformatica().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorInformatica");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-informatica")]
        public virtual ActionResult BuscarPorInformatica(KnowledgeInformatic informatica)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorInformatica(User.Identity.Name,
                informatica.Software, informatica.EnumLevel);

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-idioma")]
        public virtual ActionResult BuscarPorIdioma()
        {
            ViewBag.niveis = new EnumService().ListarNivelIdioma().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.idiomas = new EnumService().ListarIdiomas().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorIdioma");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-idioma")]
        public virtual ActionResult BuscarPorIdioma(KnowledgeLanguage idioma)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorIdioma(User.Identity.Name,
                idioma.EnumLanguage, idioma.EnumConversation, idioma.EnumReading, idioma.EnumWriting);

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-atividade-extra")]
        public virtual ActionResult BuscarPorAtividadeExtra()
        {
            ViewBag.tiposAtividade = new EnumService().ListarTiposAtividade().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorAtividadeExtra");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-atividade-extra")]
        public virtual ActionResult BuscarPorAtividadeExtra(ExtraActivity atividade)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorAtividadeExtra(User.Identity.Name,
                atividade.EnumTypeActivity, atividade.EnumCountry, atividade.NameCityCountry, atividade.City);

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-curso-profissionalizante")]
        public virtual ActionResult BuscarPorCursoProfissionalizante()
        {
            ViewBag.estados = new EnumService().ListarEstados().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.paises = new EnumService().ListarPaises().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorCursoProfissonalizante");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-curso-profissionalizante")]
        public virtual ActionResult BuscarPorCursoProfissionalizante(ProfessionalCourse curso)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorCursoProfissionalizante(User.Identity.Name,
                    curso.Course, curso.Institute, curso.EnumCountry, curso.NameCityCountry, curso.City);

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }

        [LoadData, RequiresAuthorization, ActionName("buscar-por-objetivos")]
        public virtual ActionResult BuscarPorObjetivos()
        {
            ViewBag.areasInteresse = new EnumService().ListarAreasInteresse().ToSelectList(x => x, x => x).SelectValues();
            ViewBag.faixasSalario = new EnumService().ListarSalarios().ToSelectList(x => x, x => x).SelectValues();
            return View("BuscarPorObjetivo");
        }

        [HttpPost, LoadData, RequiresAuthorization, ActionName("buscar-por-objetivos")]
        public virtual ActionResult BuscarPorObjetivos(Curriculum curriculo)
        {
            var curriculos = new SearchCurriculumsService().BuscarCurriculosPorObjetivos(User.Identity.Name, curriculo.EnumFirstAreaInterest, curriculo.EnumSecondAreaInterest, curriculo.EnumThirdAreaInterest,
                curriculo.EnumSalaryRequeriments);

            return PartialView("_resultado-busca", curriculos.CurriculumsList);
        }
    }
}