// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace WebserviceNimbeHunterRH.Controllers
{
    public partial class BuscarController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public BuscarController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected BuscarController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }


        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public BuscarController Actions { get { return MVC.Buscar; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Buscar";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Buscar";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string BuscarPorFormacaoAcademica = "BuscarPorFormacaoAcademica";
            public readonly string BuscarPorExperienciaProfissional = "buscar-por-experiencia-profissional";
            public readonly string BuscarPorCertificacao = "BuscarPorCertificacao";
            public readonly string BuscarPorDadosPessoais = "buscar-por-dados-pessoais";
            public readonly string BuscarPorInformatica = "buscar-por-informatica";
            public readonly string BuscarPorIdioma = "buscar-por-idioma";
            public readonly string BuscarPorAtividadeExtra = "buscar-por-atividade-extra";
            public readonly string BuscarPorCursoProfissionalizante = "buscar-por-curso-profissionalizante";
            public readonly string BuscarPorObjetivos = "buscar-por-objetivos";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string BuscarPorFormacaoAcademica = "BuscarPorFormacaoAcademica";
            public const string BuscarPorExperienciaProfissional = "buscar-por-experiencia-profissional";
            public const string BuscarPorCertificacao = "BuscarPorCertificacao";
            public const string BuscarPorDadosPessoais = "buscar-por-dados-pessoais";
            public const string BuscarPorInformatica = "buscar-por-informatica";
            public const string BuscarPorIdioma = "buscar-por-idioma";
            public const string BuscarPorAtividadeExtra = "buscar-por-atividade-extra";
            public const string BuscarPorCursoProfissionalizante = "buscar-por-curso-profissionalizante";
            public const string BuscarPorObjetivos = "buscar-por-objetivos";
        }


        static readonly ActionParamsClass_BuscarPorFormacaoAcademica s_params_BuscarPorFormacaoAcademica = new ActionParamsClass_BuscarPorFormacaoAcademica();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorFormacaoAcademica BuscarPorFormacaoAcademicaParams { get { return s_params_BuscarPorFormacaoAcademica; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorFormacaoAcademica
        {
            public readonly string formacao = "formacao";
        }
        static readonly ActionParamsClass_BuscarPorExperienciaProfissional s_params_BuscarPorExperienciaProfissional = new ActionParamsClass_BuscarPorExperienciaProfissional();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorExperienciaProfissional BuscarPorExperienciaProfissionalParams { get { return s_params_BuscarPorExperienciaProfissional; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorExperienciaProfissional
        {
            public readonly string experiencia = "experiencia";
        }
        static readonly ActionParamsClass_BuscarPorCertificacao s_params_BuscarPorCertificacao = new ActionParamsClass_BuscarPorCertificacao();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorCertificacao BuscarPorCertificacaoParams { get { return s_params_BuscarPorCertificacao; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorCertificacao
        {
            public readonly string certificacao = "certificacao";
        }
        static readonly ActionParamsClass_BuscarPorDadosPessoais s_params_BuscarPorDadosPessoais = new ActionParamsClass_BuscarPorDadosPessoais();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorDadosPessoais BuscarPorDadosPessoaisParams { get { return s_params_BuscarPorDadosPessoais; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorDadosPessoais
        {
            public readonly string usuario = "usuario";
        }
        static readonly ActionParamsClass_BuscarPorInformatica s_params_BuscarPorInformatica = new ActionParamsClass_BuscarPorInformatica();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorInformatica BuscarPorInformaticaParams { get { return s_params_BuscarPorInformatica; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorInformatica
        {
            public readonly string informatica = "informatica";
        }
        static readonly ActionParamsClass_BuscarPorIdioma s_params_BuscarPorIdioma = new ActionParamsClass_BuscarPorIdioma();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorIdioma BuscarPorIdiomaParams { get { return s_params_BuscarPorIdioma; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorIdioma
        {
            public readonly string idioma = "idioma";
        }
        static readonly ActionParamsClass_BuscarPorAtividadeExtra s_params_BuscarPorAtividadeExtra = new ActionParamsClass_BuscarPorAtividadeExtra();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorAtividadeExtra BuscarPorAtividadeExtraParams { get { return s_params_BuscarPorAtividadeExtra; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorAtividadeExtra
        {
            public readonly string atividade = "atividade";
        }
        static readonly ActionParamsClass_BuscarPorCursoProfissionalizante s_params_BuscarPorCursoProfissionalizante = new ActionParamsClass_BuscarPorCursoProfissionalizante();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorCursoProfissionalizante BuscarPorCursoProfissionalizanteParams { get { return s_params_BuscarPorCursoProfissionalizante; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorCursoProfissionalizante
        {
            public readonly string curso = "curso";
        }
        static readonly ActionParamsClass_BuscarPorObjetivos s_params_BuscarPorObjetivos = new ActionParamsClass_BuscarPorObjetivos();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_BuscarPorObjetivos BuscarPorObjetivosParams { get { return s_params_BuscarPorObjetivos; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_BuscarPorObjetivos
        {
            public readonly string curriculo = "curriculo";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string _resultado_busca = "_resultado-busca";
                public readonly string BuscarPorAtividadeExtra = "BuscarPorAtividadeExtra";
                public readonly string BuscarPorCertificacao = "BuscarPorCertificacao";
                public readonly string BuscarPorCursoProfissonalizante = "BuscarPorCursoProfissonalizante";
                public readonly string BuscarPorDadosPessoais = "BuscarPorDadosPessoais";
                public readonly string BuscarPorExperienciaProfissional = "BuscarPorExperienciaProfissional";
                public readonly string BuscarPorFormacaoAcademica = "BuscarPorFormacaoAcademica";
                public readonly string BuscarPorIdioma = "BuscarPorIdioma";
                public readonly string BuscarPorInformatica = "BuscarPorInformatica";
                public readonly string BuscarPorObjetivo = "BuscarPorObjetivo";
            }
            public readonly string _resultado_busca = "~/Views/Buscar/_resultado-busca.cshtml";
            public readonly string BuscarPorAtividadeExtra = "~/Views/Buscar/BuscarPorAtividadeExtra.cshtml";
            public readonly string BuscarPorCertificacao = "~/Views/Buscar/BuscarPorCertificacao.cshtml";
            public readonly string BuscarPorCursoProfissonalizante = "~/Views/Buscar/BuscarPorCursoProfissonalizante.cshtml";
            public readonly string BuscarPorDadosPessoais = "~/Views/Buscar/BuscarPorDadosPessoais.cshtml";
            public readonly string BuscarPorExperienciaProfissional = "~/Views/Buscar/BuscarPorExperienciaProfissional.cshtml";
            public readonly string BuscarPorFormacaoAcademica = "~/Views/Buscar/BuscarPorFormacaoAcademica.cshtml";
            public readonly string BuscarPorIdioma = "~/Views/Buscar/BuscarPorIdioma.cshtml";
            public readonly string BuscarPorInformatica = "~/Views/Buscar/BuscarPorInformatica.cshtml";
            public readonly string BuscarPorObjetivo = "~/Views/Buscar/BuscarPorObjetivo.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_BuscarController : WebserviceNimbeHunterRH.Controllers.BuscarController
    {
        public T4MVC_BuscarController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            IndexOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorFormacaoAcademicaOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorFormacaoAcademica()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorFormacaoAcademica);
            BuscarPorFormacaoAcademicaOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorFormacaoAcademicaOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.AcademicFormation formacao);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorFormacaoAcademica(WebserviceNimbleHunterLibraries.Models.AcademicFormation formacao)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorFormacaoAcademica);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "formacao", formacao);
            BuscarPorFormacaoAcademicaOverride(callInfo, formacao);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorExperienciaProfissionalOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorExperienciaProfissional()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorExperienciaProfissional);
            BuscarPorExperienciaProfissionalOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorExperienciaProfissionalOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.ProfessionalExperience experiencia);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorExperienciaProfissional(WebserviceNimbleHunterLibraries.Models.ProfessionalExperience experiencia)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorExperienciaProfissional);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "experiencia", experiencia);
            BuscarPorExperienciaProfissionalOverride(callInfo, experiencia);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorCertificacaoOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorCertificacao()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorCertificacao);
            BuscarPorCertificacaoOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorCertificacaoOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.Certificate certificacao);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorCertificacao(WebserviceNimbleHunterLibraries.Models.Certificate certificacao)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorCertificacao);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "certificacao", certificacao);
            BuscarPorCertificacaoOverride(callInfo, certificacao);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorDadosPessoaisOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorDadosPessoais()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorDadosPessoais);
            BuscarPorDadosPessoaisOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorDadosPessoaisOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.User usuario);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorDadosPessoais(WebserviceNimbleHunterLibraries.Models.User usuario)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorDadosPessoais);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "usuario", usuario);
            BuscarPorDadosPessoaisOverride(callInfo, usuario);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorInformaticaOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorInformatica()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorInformatica);
            BuscarPorInformaticaOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorInformaticaOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.KnowledgeInformatic informatica);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorInformatica(WebserviceNimbleHunterLibraries.Models.KnowledgeInformatic informatica)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorInformatica);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "informatica", informatica);
            BuscarPorInformaticaOverride(callInfo, informatica);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorIdiomaOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorIdioma()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorIdioma);
            BuscarPorIdiomaOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorIdiomaOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.KnowledgeLanguage idioma);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorIdioma(WebserviceNimbleHunterLibraries.Models.KnowledgeLanguage idioma)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorIdioma);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "idioma", idioma);
            BuscarPorIdiomaOverride(callInfo, idioma);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorAtividadeExtraOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorAtividadeExtra()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorAtividadeExtra);
            BuscarPorAtividadeExtraOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorAtividadeExtraOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.ExtraActivity atividade);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorAtividadeExtra(WebserviceNimbleHunterLibraries.Models.ExtraActivity atividade)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorAtividadeExtra);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "atividade", atividade);
            BuscarPorAtividadeExtraOverride(callInfo, atividade);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorCursoProfissionalizanteOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorCursoProfissionalizante()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorCursoProfissionalizante);
            BuscarPorCursoProfissionalizanteOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorCursoProfissionalizanteOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.ProfessionalCourse curso);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorCursoProfissionalizante(WebserviceNimbleHunterLibraries.Models.ProfessionalCourse curso)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorCursoProfissionalizante);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "curso", curso);
            BuscarPorCursoProfissionalizanteOverride(callInfo, curso);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorObjetivosOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorObjetivos()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorObjetivos);
            BuscarPorObjetivosOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void BuscarPorObjetivosOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, WebserviceNimbleHunterLibraries.Models.Curriculum curriculo);

        [NonAction]
        public override System.Web.Mvc.ActionResult BuscarPorObjetivos(WebserviceNimbleHunterLibraries.Models.Curriculum curriculo)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.BuscarPorObjetivos);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "curriculo", curriculo);
            BuscarPorObjetivosOverride(callInfo, curriculo);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
