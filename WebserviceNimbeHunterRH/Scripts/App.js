﻿var App = function () {

    var addAlertToPage = function (message, type) {
        if (!toastr) {
            return;
        }

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        var $toast = toastr[type](message, null);

        return false;
    }



    return {
        init: function () {

        },

        initAddAlertToPage: function (message, type) {
            addAlertToPage(message, type);
        },

        blockUI: function (options) {
            var options = $.extend(true, {}, options);
            var html = '';
            if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="https://apautaon.blob.core.windows.net/cdn/loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'Carregando...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="https://apautaon.blob.core.windows.net/cdn/loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : 'Carregando...') + '</span></div>';
            }

            if (options.target) {
                var el = jQuery(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 100000,
                    centerY: options.cenrerY != undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else {
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 100000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                        opacity: options.boxed ? 0.1 : 0.1,
                        cursor: 'wait'
                    }
                });
            }
        },

        unblockUI: function (target) {
            if (target) {
                jQuery(target).unblock({
                    onUnblock: function () {
                        jQuery(target).css('position', '');
                        jQuery(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        }
    };

}();

$(".link-vaga").click(function (e) {
    e.preventDefault();
    var url = $(this).Attr("href");
    var idVaga = $(this).Attr("id-vancancy");
    $.ajax({
        type: 'GET',
        url: url,
        data: { 'idVaga': idVaga }
    }).done(function (data) {
        $("#modal-vaga").empty().html(data);
        $("#modal-vaga").fadeIn();
    });
});

$("body").on("click", ".ajax-modal", function (e) {
        e.preventDefault();
        console.info("entrou");
        var url = $(this).attr("href");
        if (url != null && url != "") {
            App.blockUI();
            $.ajax({
                type: "GET",
                url: url,
                cache: false,
                success: function (data) {
                    if (data != false) {
                        App.unblockUI();
                        $("#modal").empty().html(data);
                        $("#myModal").modal("show");
                    }
                }
            });
        }
    });

$(".form-ajax").submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var serialize = $(form).serialize();
    var url = $(form).attr("action");
    App.blockUI();
    $.ajax({
        type: 'POST',
        url: url,
        data: serialize
    }).done(function (data) {
        console.info("entrou");
        App.unblockUI();
        $("#resultado-busca").empty().html(data);
    });
});


$("#idEtapa").change(function () {
    console.info("entrou");
    var idEtapa = $(this).val();
    console.info(idEtapa);
    var idVaga = $("#idVaga").val();
    console.info(idVaga);
    window.location.href = "/webservicenimblehunterrh/ProcessosSeletivos/Historico" + "?idVaga=" + idVaga + "&idEtapa=" + idEtapa;
});

$("#enviarEmail").change(function () {
    if ($(this).is(":checked")) {
        $(".mensagens").fadeIn();
    } else {
        $(".mensagens").fadeOut();
    }
});