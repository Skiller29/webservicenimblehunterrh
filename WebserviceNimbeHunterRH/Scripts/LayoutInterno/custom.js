
    $(".select-estado").change(function () {
        var estado = $(this).val();
        $.ajax({
            type: 'GET',
            url: 'http://www.homolog.webroad.com.br/nimblehunterrh/api/listarcidades',
            data: { 'estado': estado }
        }).done(function (data) {
            $(".select-cidade").removeAttr("disabled");
            $(".select-cidade").empty();
            $.each(data, function (i, item) {
                $(".select-cidade").append($('<option></option>').val(item.name).text(item.name));
            });
        });
    });

    $(".select-paises").change(function () {
        if ($(this).val() === "Brasil") {
            $("#NameCityCountry").val("");
            $(".box-cidades-estrangeiras").hide();
            $(".box-cidades-brasileiras").show();
        } else {
            $(".box-cidades-estrangeiras").show();
            $(".box-cidades-brasileiras").hide();
        }
    });

    $(".aprovar-candidato").change(function() {
        var aprovados = $('.aprovar-candidato:checked').length > 0;
        if (aprovados) {
            $(".form-etapa").show();
        } else {
            $(".form-etapa").hide();
        }

    });

    $("#WorkedCompany").change(function() {
        var check = $(this).is(":checked");
        if (check) {
            $(".box-trabalhou-empresa").fadeIn();
        } else {
            $(".box-trabalhou-empresa").fadeOut();
            $(".box-trabalhou-empresa input").val("");           
        }
    });

    $("#HasKinshipWorked").change(function () {
        var check = $(this).is(":checked");
        if (check) {
            $(".box-parente-empresa").fadeIn();
        } else {
            $(".box-parente-empresa").fadeOut();
            $(".box-parente-empresa input").val("");
            $(".box-parente-empresa select").val("Selecione o grau de parentesco");
        }
    });

